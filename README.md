## Getting Started

From the project root directory, simply:

`npm install`

And wait a while... then:

`npm run dev:api` // REST API for the Taskr application

`npm run dev:client` // UI and simple web server to run and build the presentation, example components and the Taskr application

Once the dev:client web server is running, Go to:
http://localhost:8009



The code is split into two main areas:

- Example Components (/taskr/#/examples)
    - Pages: ./src/client/app/js/views/pages/examples/
    - Components: ./src/client/app/js/views/components/examples/
    
- The "Taskr" application
    - Pages: ./src/client/app/js/views/pages/taskr/
    - Components: ./src/client/app/js/views/components/taskr/
    
