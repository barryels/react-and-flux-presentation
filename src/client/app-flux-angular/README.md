# Web UI

## Structure

```
                    +------------+
                    |            |
             +------>   Action   +------+
             |      |            |      |
             |      +------------+      |
             |                          |
+------------+------------+     +-------v--------+
|                         |     |                |
|  Directive / Controller |     |   Dispatcher   |
|                         |     |                |
+------------A------------+     +-------+--------+
             |                          |
             |       +-----------+      |
             |       |           |      |
             +-------+  Store(s) <------+
                     |           |
                     +---A---+---+ 
                         |   |
                         |   |
             +-----------+---V----------+
             |                          |
             |         Manifest         |
             |                          |
             +-----------A---+----------+
                         |   |
                         |   |
                 +-------+---V------+
                 |                  |
                 |   Back-end API   |
                 |                  |
                 +------------------+
```




### How to Install, Build & Run
* In Terminal / Command Prompt, **'cd'** to the **"./app/web-ui/"** directory
* Run `npm install`
* Wait for modules to download
* To build the dev bundles & watch for changes, run: `gulp`
* Open the 'ui.html' file in your browser