(function(module) {
try {
  module = angular.module('App');
} catch (e) {
  module = angular.module('App', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('PageDashboard/PageDashboard-view.html',
    '<h1>Dashboard</h1>\n' +
    '\n' +
    '<thing-list>itemlist</thing-list>\n' +
    '\n' +
    '<thing-new>thing-new</thing-new>\n' +
    '\n' +
    '<total-things-badge>total-items-badge</total-things-badge>');
}]);
})();

(function(module) {
try {
  module = angular.module('App');
} catch (e) {
  module = angular.module('App', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('PageThingDetail/PageThingDetail-view.html',
    '<thing-detail id="itemId">item-detail</thing-detail>\n' +
    '\n' +
    '<total-things-badge>total-items-badge</total-things-badge>');
}]);
})();

(function(module) {
try {
  module = angular.module('App');
} catch (e) {
  module = angular.module('App', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('PageThingNew/PageThingNew-view.html',
    '<button type="button" ng-disabled="state.isUpdatingData" ng-click="this.handleClickBackButton()">Back to Dasboard</button>\n' +
    '\n' +
    '<thing-new>thing-new</thing-new>\n' +
    '\n' +
    '<total-things-badge>total-items-badge</total-things-badge>');
}]);
})();
