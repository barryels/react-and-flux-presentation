angular.module('App', ['flux', 'ngRoute']);
angular.module('App')
    .factory('Manifest', ['MOCK_API', function (MOCK_API) {
        'use strict';

        var Manifest;

        /*
         * This object abstracts away any and all external data requests, 
         * so that the UI can be built and tested without worrying about another system being available / online.
         * 
         * Some cool things it can do:
         * 1. Use a mock API for local testing
         * 2. Simulate server response delays through the getDelayDuration() method
         * 3. Mutate data from an external API into a UI compatible data format (Think XML parsing, etc.).
         * 4. Mutate date sent from the UI into an API compatible structure.
         * 5. Call multiple external APIs, concatenate their responses and return a single response to the UI
         */
        Manifest = {

            useMock: true,

            /* --- API utility methods --- */

            /*
             * Change the return value to a positive integer (in milliseconds) to mock slow / fast API calls.
             */
            getDelayDuration: function () {
                //return 0; // Fastest 0s
                //return (Math.floor(Math.random() * 100) + 100); // Faster: > 0.1s < 0.2s // Expected result
                return (Math.floor(Math.random() * 250) + 250); // Fast: > 0.25s < 0.5s
                //return (Math.floor(Math.random() * 500) + 500); // Slow: > 0.5s < 1s
                //return (Math.floor(Math.random() * 1000) + 1000); // Slower: > 1s < 2s
                //return (Math.floor(Math.random() * 2000) + 2000); // Slowest: > 2s < 4s
            },

            createAPIRequest: function (name, type, url, data, dataType, headers) {
                var request = {
                    name: name,
                    type: type,
                    //url: url + "?rand=" + Math.random(),
                    url: url,
                    data: data,
                    dataType: dataType,
                    headers: headers
                };

                if (request.headers === undefined) {
                    request.headers = {
                        "Accept": "application/json"
                    };
                }

                if (request.dataType === undefined) {
                    request.dataType = "json";
                }

                if (request.type === undefined) {
                    request.type = "GET";
                }

                return request;
            },

            apiCall: function (request, delayDuration) {
                var ajax,
                    ajaxDelayed = {},
                    ajaxDelayedScopeProps = {};

                if (delayDuration === undefined) {
                    delayDuration = 0;
                }

                ajax = $.Deferred();

                ajaxDelayedScopeProps = {
                    "ajaxDelayed": ajaxDelayed,
                    "ajax": ajax,
                    "request": request,
                    "useMock": this.useMock
                };

                if (this.useMock === true) {
                    // Use the mock data store endpoints
                    setTimeout(function (scopeProps) {
                        scopeProps.ajaxDelayed = MOCK_API.apiCall(scopeProps.request)
                            .done(function (response) {
                                scopeProps.ajax.resolve(response);
                            });
                    }, delayDuration, ajaxDelayedScopeProps);
                } else {
                    // Use the live API endpoints
                    setTimeout(function (scopeProps) {
                        scopeProps.ajaxDelayed = $.ajax(scopeProps.request)
                            .done(function (response) {
                                scopeProps.ajax.resolve(response);
                            });
                    }, delayDuration, ajaxDelayedScopeProps);
                }

                return ajax;
            },



            /* --- API method wrappers --- */

            /*
             * Get all the Things
             * Returns a promise object
             */
            readAllThings: function () {
                var d;

                d = this.apiCall(this.createAPIRequest("readAllThings", "GET", "/api/things/", null), this.getDelayDuration())
                    .pipe(function (response) {
                        return response;
                    });

                return d.promise();

            },

            /*
             * Create a new Thing
             * @param {object} data
             */
            createThing: function (data) {
                var d;

                // Ensure there isn't an 'id' prop present, if so, delete it. The API will generate its own id.
                delete data.id;

                d = this.apiCall(this.createAPIRequest("createThing", "POST", "/api/thing/", data), this.getDelayDuration())
                    .pipe(function (response) {
                        return response;
                    });
                return d.promise();
            },

            /*
             * Get single Thing
             * @param {string} id
             */
            readThing: function (id) {
                var d,
                    data = {};

                data.id = id;
                d = this.apiCall(this.createAPIRequest("readThing", "GET", "/api/thing/" + id, data), this.getDelayDuration())
                    .pipe(function (response) {
                        return response;
                    });
                return d.promise();
            },

            /*
             * Update single Thing
             * @param {object} data
             */
            updateThing: function (data) {
                var d;

                d = this.apiCall(this.createAPIRequest("updateThing", "PUT", "/api/thing/" + data.id, data), this.getDelayDuration())
                    .pipe(function (response) {
                        return response;
                    });

                return d.promise();

            },

            /*
             * Delete single Thing
             * @param {string} id
             */
            deleteThing: function (id) {
                var d;

                d = this.apiCall(this.createAPIRequest("deleteThing", "DELETE", "/api/thing/" + id, {"id": id}), this.getDelayDuration())
                    .pipe(function (response) {
                        return response;
                    });

                return d.promise();

            }

        };

        return Manifest;

    }]);

angular.module('App')
    .config(function ($routeProvider, $locationProvider) {

        'use strict';

        function getPage(pageName) {
            return pageName + '/' + pageName + '-view.html';
        };

        $routeProvider
            .when('/dashboard', {
                templateUrl: getPage('PageDashboard'),
                controller: 'PageDashboard'
            })
            .when('/thing/:id', {
                templateUrl: getPage('PageThingDetail'),
                controller: 'PageThingDetail'
            })
            .when('/thing-create', {
                templateUrl: getPage('PageThingNew'),
                controller: 'PageThingNew'
            })
            .otherwise({
                redirectTo: '/dashboard'
            });
    });

angular.module('App')
    .store('ThingStore', ['flux', 'Manifest', function (flux, Manifest) {

        'use strict';

        return {

            collection: [],

            handlers: {
                'thing:create': 'create',
                'thing:read': 'read',
                'thing:update': 'update',
                'thing:delete': 'remove',
                'thing:readAll': 'readAll',
                'thing:readLastDeleted': 'readLastDeleted'
            },


            readAll: function () {
                var self = this;

                Manifest.readAllThings()
                    .done(function (response) {
                        self.collection = response;
                        self.emitChange();
                    });
            },

            create: function (data) {
                var self = this;

                this.emit('thing:create:progress');

                data.title = data.title ? data.title : "Placeholder " + Math.round(Math.random() * 10);

                Manifest.createThing(data)
                    .done(function (response) {
                        if (response !== null) {
                            self.emit("thing:create:success", response);
                            self.readAll();
                            //self.emitChange();
                        } else {
                            self.emit("thing:create:fail", response);
                        }
                    });
            },

            read: function (id) {
                var self = this;

                this.emit('thing:read:progress');

                Manifest.readThing(id)
                    .done(function (response) {
                        if (response !== null) {
                            self.emit("thing:read:success", response);
                        } else {
                            self.emit("thing:read:fail", response);
                        }
                    });
            },

            update: function (data) {
                var self = this;

                this.emit('thing:update:progress', data);

                Manifest.updateThing(data)
                    .done(function (response) {
                        if (response !== null) {
                            self.emit("thing:update:success", response);
                            self.readAll();
                        } else {
                            self.emit("thing:update:fail", response);
                        }
                    });
            },

            remove: function (id) {
                var self = this;

                this.emit('thing:delete:progress');

                Manifest.deleteThing(id)
                    .done(function (response) {
                        if (response !== null) {
                            self.emit("thing:delete:success", response);
                            self.readAll();
                        } else {
                            self.emit("thing:delete:fail", response);
                        }
                    });
            },

            exports: {
                collection: function () {
                    return this.collection;
                }
            }

        };
    }]);

angular.module('App')
    .controller('PageDashboard', ['$scope', 'flux', function ($scope, flux) {
        'use strict';

    }]);
angular.module('App')
    .controller('PageThingDetail', ['$scope', '$routeParams', function ($scope, $routeParams) {
        'use strict';

        $scope.itemId = null;

        this.init = function () {
            $scope.itemId = $routeParams.id;
        };

        this.init();

    }]);
angular.module('App')
    .controller('PageThingNew', ['$scope', 'flux', 'ThingStore', '$location', function ($scope, flux, ThingStore, $locationProvider) {
        'use strict';

        $scope.state = {
            isUpdatingData: false
        };

        this.init = function () {
        };

        $scope.handleClickBackButton = function () {
            $locationProvider.path('/dashboard');
        };

        this.init();

    }]);
angular.module('App')
    .controller('AppController', ['Manifest', function (Manifest) {

        'use strict';

        // Do some app-wide stuff here

    }]);
angular.module('App')
    .directive('thingDetail', [function () {
        'use strict';

        return {
            restrict: 'E',
            templateUrl: 'ThingDetail/ThingDetail.html',
            scope: {
                id: '='
            },
            controller: ['$scope', 'flux', 'ThingStore', '$location', function ($scope, flux, ThingStore, $locationProvider) {

                $scope.state = {
                    isUpdatingData: false,
                    item: null
                };

                this.init = function () {
                    $scope.readItem($scope.id);
                };


                // UI event handlers
                $scope.handleClickBackButton = function () {
                    $locationProvider.path('/dashboard');
                };

                $scope.handleClickUpdateButton = function () {
                    this.updateItem();
                };

                $scope.handleClickDeleteButton = function () {
                    this.deleteItem();
                };


                // Action Dsipatchers
                $scope.readItem = function (itemId) {
                    flux.dispatch('thing:read', itemId);
                };

                $scope.updateItem = function () {
                    var data = {
                        id: $scope.state.item.id,
                        title: $scope.state.item.title
                    };
                    flux.dispatch('thing:update', data);
                };

                $scope.deleteItem = function () {
                    var id;

                    id = $scope.state.item.id;
                    flux.dispatch('thing:delete', id);
                };


                $scope.safeApply = function () {
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }

                // Event Listeners
                $scope.$listenTo(ThingStore, function () {
                    $scope.safeApply();
                });


                $scope.$listenTo(ThingStore, 'thing:read:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:read:success', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.item = item;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:read:fail', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.item = null;
                    $scope.safeApply();
                });


                $scope.$listenTo(ThingStore, 'thing:update:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                });
                $scope.$listenTo(ThingStore, 'thing:update:success', function (item) {
                    $scope.state.isUpdatingData = false;
                });
                $scope.$listenTo(ThingStore, 'thing:update:fail', function (item) {
                    $scope.state.isUpdatingData = false;
                });


                $scope.$listenTo(ThingStore, 'thing:delete:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                });
                $scope.$listenTo(ThingStore, 'thing:delete:success', function (item) {
                    $scope.state.isUpdatingData = false;
                });
                $scope.$listenTo(ThingStore, 'thing:delete:fail', function (item) {
                    $scope.state.isUpdatingData = false;
                });

                this.init();

            }]
        };
    }]);
angular.module('App')
    .directive('thingList', [function () {
        'use strict';

        return {
            restrict: 'E',
            templateUrl: 'ThingList/ThingList.html',

            controller: ['$scope', 'flux', 'ThingStore', '$location', function ($scope, flux, ThingStore, $locationProvider, $render) {

                $scope.props = {
                    totalItemsText_Reading: "(Loading things)",
                    totalItemsText_Creating: "(Creating new thing)",
                    totalItemsText_Success: "Success!",
                    totalItemsText_Fail: "(Last action failed)"
                };

                $scope.state = {
                    collection: [],//ThingStore.collection()
                    totalItemsText: $scope.props.totalItemsText_Reading,
                    isUpdatingData: false
                };

                this.init = function () {
                    $scope.readAll();
                };

                /* Handlers */
                $scope.handleClickDeleteButton = function (itemId) {
                    this.deleteItem(itemId);
                };

                $scope.handleClickViewButton = function (itemId) {
                    $locationProvider.path('/thing/' + itemId);
                };


                /* Action Dsipatchers */
                $scope.readAll = function () {
                    flux.dispatch('thing:readAll');
                };

                $scope.viewItem = function (itemId) {
                    flux.dispatch('thing:view', itemId);
                };

                $scope.deleteItem = function (itemId) {
                    var shouldDelete = window.confirm("Are you sure you wish to delete this item?");
                    if (shouldDelete === true) {
                        flux.dispatch('thing:delete', itemId);
                    } else {
                        // User cancelled the delete operation.
                        // Do nothing, or perhaps display a nice message saying "Thank you for not deleting me :)"
                    }
                };

                $scope.createItem = function (title) {
                    var data = {
                        title: title
                    };
                    flux.dispatch('thing:create', data);
                };

                $scope.safeApply = function () {
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                };

                /* Event Listeners */
                $scope.$listenTo(ThingStore, function () {
                    //$scope.state.isUpdatingData = false;
                    $scope.state.collection = ThingStore.collection();
                    $scope.safeApply();
                });


                $scope.$listenTo(ThingStore, 'thing:readAll:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Reading;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:readAll:success', function (collection) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.collection = ThingStore.collection();
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Success;
                    $scope.safeApply();
                });


                $scope.$listenTo(ThingStore, 'thing:create:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Creating;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:create:success', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Success;
                    //$scope.readAll();
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:create:fail', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Fail;
                    $scope.safeApply();
                });


                $scope.$listenTo(ThingStore, 'thing:delete:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Deleting;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:delete:success', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Success;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:delete:fail', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Fail;
                    $scope.safeApply();
                });

                this.init();

            }]
        };
    }]);
angular.module('App')
    .directive('thingNew', [function () {
        'use strict';

        return {
            restrict: 'E',
            templateUrl: 'ThingNew/ThingNew.html',
            scope: {
                id: '='
            },
            controller: ['$scope', 'flux', 'ThingStore', '$location', function ($scope, flux, ThingStore, $locationProvider) {

                $scope.state = {
                    isUpdatingData: false,
                    item: null
                };

                this.init = function () {
                    
                };


                // UI event handlers
                $scope.handleClickCreateNewButton = function () {
                    $scope.createItem($scope.state.itemTitle);
                };

                $scope.createItem = function (title) {
                    var data = {
                        title: title
                    };
                    flux.dispatch('thing:create', data);
                };


                $scope.safeApply = function () {
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                };


                $scope.$listenTo(ThingStore, 'thing:create:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                });

                $scope.$listenTo(ThingStore, 'thing:create:success', function (item) {

                    $scope.state.isUpdatingData = false;

                    var shouldGoToItem = window.confirm("Successfully created, would you like to go to the item update screen?");
                    if (shouldGoToItem === true) {
                        console.info($locationProvider.path);
                        $locationProvider.path('/thing/' + String(item.id));
                    } else {
                        $scope.state.itemTitle = '';
                    }

                    $scope.safeApply();
                });

                $scope.$listenTo(ThingStore, 'thing:create:fail', function (item) {
                    alert("Create failed");
                    $scope.state.isUpdatingData = false;
                });

                this.init();

            }]
        };
    }]);
angular.module('App')
    .directive('totalThingsBadge', [function () {
        'use strict';

        return {
            restrict: 'E',
            templateUrl: 'TotalThingsBadge/TotalThingsBadge.html',

            controller: ['$scope', 'flux', 'ThingStore', function ($scope, flux, ThingStore) {

                $scope.state = {
                    total: ThingStore.collection().length
                };

                this.init = function () {
                    $scope.readAll();
                };

                $scope.readAll = function () {
                    flux.dispatch('thing:readAll');
                };

                $scope.safeApply = function () {
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }

                /* Event Listeners */
                $scope.$listenTo(ThingStore, function () {
                    $scope.state.total = ThingStore.collection().length;
                    $scope.safeApply();
                });

                this.init();

            }]
        };
    }]);
angular.module('App')
    .factory('MOCK_API', ['MOCK_DATA', function (MOCK_DATA) {

        'use strict';

        var MOCK_API,
            MOCK_DATA;

        MOCK_API = {

            doRandomFail: false,

            MOCK_STORES: {
                things: depot('things', { idAttribute: 'id' }),
                users: depot('users', { idAttribute: 'id' }),
                things_users: depot('things_users', { idAttribute: 'id' })
            },

            initiliaseData: function () {
                var i = 0,
                    j = 0,
                    thing = {},
                    user = {};

                //depot('things').destroyAll();

                if (this.MOCK_STORES.things.store === null) {
                    alert("Initialising fake 'thing' data");

                    /**/
                    for (i = 0; i < MOCK_DATA.things.length; i += 1) {
                        thing = MOCK_DATA.things[i];
                        this.MOCK_STORES.things.save(thing);
                    }
                    /**/
                }

            },

            apiCall: function (request) {
                this.initiliaseData();
                if (this[request.name] !== undefined) {
                    return this[request.name](request);
                }
            },



            // Domain specific MOCK methods
            readAllThings: function (request) {
                return this.READ_COLLECTION(request, this.MOCK_STORES.things);
            },
            createThing: function (request) {
                request.data.createdAt = new Date().getTime();
                return this.CREATE_ENTITY(request, this.MOCK_STORES.things);
            },
            readThing: function (request) {
                return this.READ_ENTITY(request, this.MOCK_STORES.things);
            },
            updateThing: function (request) {
                request.data.updatedAt = new Date().getTime();
                return this.UPDATE_ENTITY(request, this.MOCK_STORES.things);
            },
            deleteThing: function (request) {
                return this.DELETE_ENTITY(request, this.MOCK_STORES.things);
            },



            // Generic CRUD methods
            READ_COLLECTION: function (request, identityStore) {
                var d = $.Deferred(),
                    response = [];

                response = identityStore.all();
                d.resolve(response);

                return d.promise();
            },

            CREATE_ENTITY: function (request, identityStore) {
                //console.info("MOCK_API.CREATE_ENTITY()", request);
                var d = $.Deferred(),
                    requestDataClone = {},
                    response = {};

                // Create deep copy of the request data object, 
                // so that we don't have any lingering object references that could bite us later
                requestDataClone = $.extend(true, {}, request.data);

                response = identityStore.save(requestDataClone);

                d.resolve(response);

                return d.promise();
            },

            READ_ENTITY: function (request, identityStore) {
                //console.info("MOCK_API.READ_ENTITY()", request);
                var d = $.Deferred(),
                    response = {};

                response = identityStore.get(request.data.id);

                d.resolve(response);

                return d.promise();
            },

            UPDATE_ENTITY: function (request, identityStore) {
                //console.info("MOCK_API.UPDATE_ENTITY()", request);
                var d = $.Deferred(),
                    requestDataClone = {},
                    response = {};

                // Create deep copy of the request data object, 
                // so that we don't have any lingering object references that could bite us later
                requestDataClone = $.extend(true, {}, request.data);

                response = identityStore.update(requestDataClone);

                d.resolve(response);

                return d.promise();
            },

            DELETE_ENTITY: function (request, identityStore) {
                //console.info("MOCK_API.DELETE_ENTITY()", request);
                var d = $.Deferred(),
                    response = {};

                response = identityStore.destroy(request.data.id);

                d.resolve(response);

                return d.promise();
            }


        };

        return MOCK_API;

    }]);
angular.module('App')
    .factory('MOCK_DATA', [function () {

        'use strict';

        return {

            // Things Data
            things: [
                {"id": 1, "title": "Thing 1", "createdAt": new Date().getTime()},
                {"id": 2, "title": "Thing 2", "createdAt": new Date().getTime()},
                {"id": 3, "title": "Thing 3", "createdAt": new Date().getTime()},
                {"id": 4, "title": "Thing 4", "createdAt": new Date().getTime()},
                {"id": 5, "title": "Thing 5", "createdAt": new Date().getTime()},
                {"id": 6, "title": "Thing 6", "createdAt": new Date().getTime()},
                {"id": 7, "title": "Thing 7", "createdAt": new Date().getTime()}
            ],

        };


    }]);