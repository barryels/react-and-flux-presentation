(function(module) {
try {
  module = angular.module('App');
} catch (e) {
  module = angular.module('App', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('ThingDetail/ThingDetail.html',
    '<div class="thing-detail">\n' +
    '    \n' +
    '    <button type="button" ng-disabled="state.isUpdatingData" ng-click="this.handleClickBackButton()">Back to Dasboard</button>\n' +
    '    \n' +
    '    <h3>{{state.item.title}}</h3>\n' +
    '    \n' +
    '    <div ng-if="state.isUpdatingData == false && state.item == null">\n' +
    '        Item with id <strong>{{id}}</strong> could not be found. It has probably been deleted.\n' +
    '    </div>\n' +
    '\n' +
    '    <div ng-if="state.item != null">\n' +
    '        <div>\n' +
    '            <label>Item Title:</label>\n' +
    '            <input type="text" ng-model="state.item.title" />\n' +
    '        </div>\n' +
    '\n' +
    '        <button type="button" ng-disabled="state.isUpdatingData" ng-click="this.handleClickUpdateButton()">Update</button>\n' +
    '        <button ng-disabled="state.isUpdatingData" ng-click="this.handleClickDeleteButton()">Delete</button>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('App');
} catch (e) {
  module = angular.module('App', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('ThingList/ThingList.html',
    '<div class="thing-list">\n' +
    '    <h2>Thing List</h2>\n' +
    '    \n' +
    '    <div ng-show="state.isUpdatingData">Updating...</div>\n' +
    '    \n' +
    '    <span>Total items: {{ state.collection.length }}</span>\n' +
    '    <ul>\n' +
    '        <li ng-repeat="item in state.collection">\n' +
    '            {{ item.title }}\n' +
    '            <br /><small>id: {{ item.id }}</small>\n' +
    '            <button ng-disabled="state.isUpdatingData" ng-click="this.handleClickViewButton(item.id)">View</button> | \n' +
    '            <button ng-disabled="state.isUpdatingData" ng-click="this.handleClickDeleteButton(item.id)">delete item</button></li>\n' +
    '    </ul>\n' +
    '    \n' +
    '    <div>\n' +
    '        <a href="#/thing-create/">Create new thing (page)</a>\n' +
    '    </div>\n' +
    '    \n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('App');
} catch (e) {
  module = angular.module('App', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('ThingNew/ThingNew.html',
    '<div class="thing-new">\n' +
    '    <input type="text" ng-model="state.itemTitle" />\n' +
    '    <button ng-disabled="state.isUpdatingData" ng-click="this.handleClickCreateNewButton()">Create New Thing</button>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('App');
} catch (e) {
  module = angular.module('App', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('TotalThingsBadge/TotalThingsBadge.html',
    '<div class="total-things-badge">\n' +
    '    <span>{{state.total}}</span>\n' +
    '</div>');
}]);
})();
