var gulp,
    path,
    concat,
    ngHtml2Js,
    //minifyHTML,
    //copy,
    //clean,
    less,
    livereload,
    _pathNodeModules,
    _pathSourceRoot,
    _pathPublicRoot;

gulp = require('gulp');
path = require('path');
concat = require('gulp-concat');
ngHtml2Js = require("gulp-ng-html2js");
//minifyHTML = require("gulp-minify-html");
//copy = require("gulp-copy");
//clean = require("gulp-clean");
less = require("gulp-less");
livereload = require("gulp-livereload");

_pathNodeModules = './node_modules/';
_pathSourceRoot = './';
_pathPublicRoot = './../../Content/web-ui/';


gulp.task('js-lib', function () {
    gulp.src([
            _pathNodeModules + 'angular/angular.js',
            _pathNodeModules + 'angular-route/angular-route.js',
            _pathNodeModules + 'flux-angular/release/flux-angular.js',
            _pathNodeModules + 'jquery/dist/jquery.js',
            _pathNodeModules + 'depot/depot.js'
        ])
        .pipe(concat('lib-bundle.js'))
        .pipe(gulp.dest(_pathPublicRoot + 'js/'));
});


gulp.task('js', function () {
    gulp.src([
            _pathSourceRoot + 'js/main.js',
            _pathSourceRoot + 'js/**/*.js'
        ])
        .pipe(concat('main-bundle.js'))
        .pipe(gulp.dest(_pathPublicRoot + 'js/'))
        .pipe(livereload());
});


gulp.task('components-html', function () {
    gulp.src(_pathSourceRoot + 'js/components/**/*.html')
        .pipe(ngHtml2Js({
            prefix: '',
            moduleName: 'App'
        }))
        .pipe(concat("components-bundle.js"))
        .pipe(gulp.dest(_pathPublicRoot + 'js/'))
        .pipe(livereload());
});

gulp.task('pages-html', function () {
    gulp.src(_pathSourceRoot + 'js/pages/**/*.html')
        .pipe(ngHtml2Js({
            prefix: '',
            moduleName: 'App'
        }))
        .pipe(concat("pages-bundle.js"))
        .pipe(gulp.dest(_pathPublicRoot + 'js/'))
        .pipe(livereload());
});


gulp.task('style', function () {
    gulp.src([
            _pathSourceRoot + 'js/style/theme.less',
            _pathSourceRoot + 'js/pages/**/*.less',
            _pathSourceRoot + 'js/components/**/*.less'
        ])
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(concat("style-bundle.css"))
        .pipe(gulp.dest(_pathPublicRoot + 'css/'))
        .pipe(livereload());
});


gulp.task('watch', function () {
    gulp.watch(_pathSourceRoot + 'js/**/*.js', ['js']);
    gulp.watch(_pathSourceRoot + 'js/pages/**/*.html', ['pages-html']);
    gulp.watch(_pathSourceRoot + 'js/components/**/*.html', ['components-html']);
    gulp.watch(_pathSourceRoot + 'js/**/*.less', ['style']);
});

gulp.task('default', ['js-lib', 'js', 'pages-html', 'components-html', 'style', 'watch']);

livereload({ start: true })