angular.module('App')
    .controller('PageThingDetail', ['$scope', '$routeParams', function ($scope, $routeParams) {
        'use strict';

        $scope.itemId = null;

        this.init = function () {
            $scope.itemId = $routeParams.id;
        };

        this.init();

    }]);