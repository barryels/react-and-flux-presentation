angular.module('App')
    .controller('PageThingNew', ['$scope', 'flux', 'ThingStore', '$location', function ($scope, flux, ThingStore, $locationProvider) {
        'use strict';

        $scope.state = {
            isUpdatingData: false
        };

        this.init = function () {
        };

        $scope.handleClickBackButton = function () {
            $locationProvider.path('/dashboard');
        };

        this.init();

    }]);