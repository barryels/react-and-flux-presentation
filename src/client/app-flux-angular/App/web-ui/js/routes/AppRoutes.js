angular.module('App')
    .config(function ($routeProvider, $locationProvider) {

        'use strict';

        function getPage(pageName) {
            return pageName + '/' + pageName + '-view.html';
        };

        $routeProvider
            .when('/dashboard', {
                templateUrl: getPage('PageDashboard'),
                controller: 'PageDashboard'
            })
            .when('/thing/:id', {
                templateUrl: getPage('PageThingDetail'),
                controller: 'PageThingDetail'
            })
            .when('/thing-create', {
                templateUrl: getPage('PageThingNew'),
                controller: 'PageThingNew'
            })
            .otherwise({
                redirectTo: '/dashboard'
            });
    });
