angular.module('App')
    .factory('MOCK_API', ['MOCK_DATA', function (MOCK_DATA) {

        'use strict';

        var MOCK_API,
            MOCK_DATA;

        MOCK_API = {

            doRandomFail: false,

            MOCK_STORES: {
                things: depot('things', { idAttribute: 'id' }),
                users: depot('users', { idAttribute: 'id' }),
                things_users: depot('things_users', { idAttribute: 'id' })
            },

            initiliaseData: function () {
                var i = 0,
                    j = 0,
                    thing = {},
                    user = {};

                //depot('things').destroyAll();

                if (this.MOCK_STORES.things.store === null) {
                    alert("Initialising fake 'thing' data");

                    /**/
                    for (i = 0; i < MOCK_DATA.things.length; i += 1) {
                        thing = MOCK_DATA.things[i];
                        this.MOCK_STORES.things.save(thing);
                    }
                    /**/
                }

            },

            apiCall: function (request) {
                this.initiliaseData();
                if (this[request.name] !== undefined) {
                    return this[request.name](request);
                }
            },



            // Domain specific MOCK methods
            readAllThings: function (request) {
                return this.READ_COLLECTION(request, this.MOCK_STORES.things);
            },
            createThing: function (request) {
                request.data.createdAt = new Date().getTime();
                return this.CREATE_ENTITY(request, this.MOCK_STORES.things);
            },
            readThing: function (request) {
                return this.READ_ENTITY(request, this.MOCK_STORES.things);
            },
            updateThing: function (request) {
                request.data.updatedAt = new Date().getTime();
                return this.UPDATE_ENTITY(request, this.MOCK_STORES.things);
            },
            deleteThing: function (request) {
                return this.DELETE_ENTITY(request, this.MOCK_STORES.things);
            },



            // Generic CRUD methods
            READ_COLLECTION: function (request, identityStore) {
                var d = $.Deferred(),
                    response = [];

                response = identityStore.all();
                d.resolve(response);

                return d.promise();
            },

            CREATE_ENTITY: function (request, identityStore) {
                //console.info("MOCK_API.CREATE_ENTITY()", request);
                var d = $.Deferred(),
                    requestDataClone = {},
                    response = {};

                // Create deep copy of the request data object, 
                // so that we don't have any lingering object references that could bite us later
                requestDataClone = $.extend(true, {}, request.data);

                response = identityStore.save(requestDataClone);

                d.resolve(response);

                return d.promise();
            },

            READ_ENTITY: function (request, identityStore) {
                //console.info("MOCK_API.READ_ENTITY()", request);
                var d = $.Deferred(),
                    response = {};

                response = identityStore.get(request.data.id);

                d.resolve(response);

                return d.promise();
            },

            UPDATE_ENTITY: function (request, identityStore) {
                //console.info("MOCK_API.UPDATE_ENTITY()", request);
                var d = $.Deferred(),
                    requestDataClone = {},
                    response = {};

                // Create deep copy of the request data object, 
                // so that we don't have any lingering object references that could bite us later
                requestDataClone = $.extend(true, {}, request.data);

                response = identityStore.update(requestDataClone);

                d.resolve(response);

                return d.promise();
            },

            DELETE_ENTITY: function (request, identityStore) {
                //console.info("MOCK_API.DELETE_ENTITY()", request);
                var d = $.Deferred(),
                    response = {};

                response = identityStore.destroy(request.data.id);

                d.resolve(response);

                return d.promise();
            }


        };

        return MOCK_API;

    }]);