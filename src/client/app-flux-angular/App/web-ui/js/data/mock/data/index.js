angular.module('App')
    .factory('MOCK_DATA', [function () {

        'use strict';

        return {

            // Things Data
            things: [
                {"id": 1, "title": "Thing 1", "createdAt": new Date().getTime()},
                {"id": 2, "title": "Thing 2", "createdAt": new Date().getTime()},
                {"id": 3, "title": "Thing 3", "createdAt": new Date().getTime()},
                {"id": 4, "title": "Thing 4", "createdAt": new Date().getTime()},
                {"id": 5, "title": "Thing 5", "createdAt": new Date().getTime()},
                {"id": 6, "title": "Thing 6", "createdAt": new Date().getTime()},
                {"id": 7, "title": "Thing 7", "createdAt": new Date().getTime()}
            ],

        };


    }]);