angular.module('App')
    .factory('Manifest', ['MOCK_API', function (MOCK_API) {
        'use strict';

        var Manifest;

        /*
         * This object abstracts away any and all external data requests, 
         * so that the UI can be built and tested without worrying about another system being available / online.
         * 
         * Some cool things it can do:
         * 1. Use a mock API for local testing
         * 2. Simulate server response delays through the getDelayDuration() method
         * 3. Mutate data from an external API into a UI compatible data format (Think XML parsing, etc.).
         * 4. Mutate date sent from the UI into an API compatible structure.
         * 5. Call multiple external APIs, concatenate their responses and return a single response to the UI
         */
        Manifest = {

            useMock: true,

            /* --- API utility methods --- */

            /*
             * Change the return value to a positive integer (in milliseconds) to mock slow / fast API calls.
             */
            getDelayDuration: function () {
                //return 0; // Fastest 0s
                //return (Math.floor(Math.random() * 100) + 100); // Faster: > 0.1s < 0.2s // Expected result
                return (Math.floor(Math.random() * 250) + 250); // Fast: > 0.25s < 0.5s
                //return (Math.floor(Math.random() * 500) + 500); // Slow: > 0.5s < 1s
                //return (Math.floor(Math.random() * 1000) + 1000); // Slower: > 1s < 2s
                //return (Math.floor(Math.random() * 2000) + 2000); // Slowest: > 2s < 4s
            },

            createAPIRequest: function (name, type, url, data, dataType, headers) {
                var request = {
                    name: name,
                    type: type,
                    //url: url + "?rand=" + Math.random(),
                    url: url,
                    data: data,
                    dataType: dataType,
                    headers: headers
                };

                if (request.headers === undefined) {
                    request.headers = {
                        "Accept": "application/json"
                    };
                }

                if (request.dataType === undefined) {
                    request.dataType = "json";
                }

                if (request.type === undefined) {
                    request.type = "GET";
                }

                return request;
            },

            apiCall: function (request, delayDuration) {
                var ajax,
                    ajaxDelayed = {},
                    ajaxDelayedScopeProps = {};

                if (delayDuration === undefined) {
                    delayDuration = 0;
                }

                ajax = $.Deferred();

                ajaxDelayedScopeProps = {
                    "ajaxDelayed": ajaxDelayed,
                    "ajax": ajax,
                    "request": request,
                    "useMock": this.useMock
                };

                if (this.useMock === true) {
                    // Use the mock data store endpoints
                    setTimeout(function (scopeProps) {
                        scopeProps.ajaxDelayed = MOCK_API.apiCall(scopeProps.request)
                            .done(function (response) {
                                scopeProps.ajax.resolve(response);
                            });
                    }, delayDuration, ajaxDelayedScopeProps);
                } else {
                    // Use the live API endpoints
                    setTimeout(function (scopeProps) {
                        scopeProps.ajaxDelayed = $.ajax(scopeProps.request)
                            .done(function (response) {
                                scopeProps.ajax.resolve(response);
                            });
                    }, delayDuration, ajaxDelayedScopeProps);
                }

                return ajax;
            },



            /* --- API method wrappers --- */

            /*
             * Get all the Things
             * Returns a promise object
             */
            readAllThings: function () {
                var d;

                d = this.apiCall(this.createAPIRequest("readAllThings", "GET", "/api/things/", null), this.getDelayDuration())
                    .pipe(function (response) {
                        return response;
                    });

                return d.promise();

            },

            /*
             * Create a new Thing
             * @param {object} data
             */
            createThing: function (data) {
                var d;

                // Ensure there isn't an 'id' prop present, if so, delete it. The API will generate its own id.
                delete data.id;

                d = this.apiCall(this.createAPIRequest("createThing", "POST", "/api/thing/", data), this.getDelayDuration())
                    .pipe(function (response) {
                        return response;
                    });
                return d.promise();
            },

            /*
             * Get single Thing
             * @param {string} id
             */
            readThing: function (id) {
                var d,
                    data = {};

                data.id = id;
                d = this.apiCall(this.createAPIRequest("readThing", "GET", "/api/thing/" + id, data), this.getDelayDuration())
                    .pipe(function (response) {
                        return response;
                    });
                return d.promise();
            },

            /*
             * Update single Thing
             * @param {object} data
             */
            updateThing: function (data) {
                var d;

                d = this.apiCall(this.createAPIRequest("updateThing", "PUT", "/api/thing/" + data.id, data), this.getDelayDuration())
                    .pipe(function (response) {
                        return response;
                    });

                return d.promise();

            },

            /*
             * Delete single Thing
             * @param {string} id
             */
            deleteThing: function (id) {
                var d;

                d = this.apiCall(this.createAPIRequest("deleteThing", "DELETE", "/api/thing/" + id, {"id": id}), this.getDelayDuration())
                    .pipe(function (response) {
                        return response;
                    });

                return d.promise();

            }

        };

        return Manifest;

    }]);
