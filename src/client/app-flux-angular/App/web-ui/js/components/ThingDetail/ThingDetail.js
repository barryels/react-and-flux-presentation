angular.module('App')
    .directive('thingDetail', [function () {
        'use strict';

        return {
            restrict: 'E',
            templateUrl: 'ThingDetail/ThingDetail.html',
            scope: {
                id: '='
            },
            controller: ['$scope', 'flux', 'ThingStore', '$location', function ($scope, flux, ThingStore, $locationProvider) {

                $scope.state = {
                    isUpdatingData: false,
                    item: null
                };

                this.init = function () {
                    $scope.readItem($scope.id);
                };


                // UI event handlers
                $scope.handleClickBackButton = function () {
                    $locationProvider.path('/dashboard');
                };

                $scope.handleClickUpdateButton = function () {
                    this.updateItem();
                };

                $scope.handleClickDeleteButton = function () {
                    this.deleteItem();
                };


                // Action Dsipatchers
                $scope.readItem = function (itemId) {
                    flux.dispatch('thing:read', itemId);
                };

                $scope.updateItem = function () {
                    var data = {
                        id: $scope.state.item.id,
                        title: $scope.state.item.title
                    };
                    flux.dispatch('thing:update', data);
                };

                $scope.deleteItem = function () {
                    var id;

                    id = $scope.state.item.id;
                    flux.dispatch('thing:delete', id);
                };


                $scope.safeApply = function () {
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }

                // Event Listeners
                $scope.$listenTo(ThingStore, function () {
                    $scope.safeApply();
                });


                $scope.$listenTo(ThingStore, 'thing:read:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:read:success', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.item = item;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:read:fail', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.item = null;
                    $scope.safeApply();
                });


                $scope.$listenTo(ThingStore, 'thing:update:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                });
                $scope.$listenTo(ThingStore, 'thing:update:success', function (item) {
                    $scope.state.isUpdatingData = false;
                });
                $scope.$listenTo(ThingStore, 'thing:update:fail', function (item) {
                    $scope.state.isUpdatingData = false;
                });


                $scope.$listenTo(ThingStore, 'thing:delete:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                });
                $scope.$listenTo(ThingStore, 'thing:delete:success', function (item) {
                    $scope.state.isUpdatingData = false;
                });
                $scope.$listenTo(ThingStore, 'thing:delete:fail', function (item) {
                    $scope.state.isUpdatingData = false;
                });

                this.init();

            }]
        };
    }]);