angular.module('App')
    .directive('thingNew', [function () {
        'use strict';

        return {
            restrict: 'E',
            templateUrl: 'ThingNew/ThingNew.html',
            scope: {
                id: '='
            },
            controller: ['$scope', 'flux', 'ThingStore', '$location', function ($scope, flux, ThingStore, $locationProvider) {

                $scope.state = {
                    isUpdatingData: false,
                    item: null
                };

                this.init = function () {
                    
                };


                // UI event handlers
                $scope.handleClickCreateNewButton = function () {
                    $scope.createItem($scope.state.itemTitle);
                };

                $scope.createItem = function (title) {
                    var data = {
                        title: title
                    };
                    flux.dispatch('thing:create', data);
                };


                $scope.safeApply = function () {
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                };


                $scope.$listenTo(ThingStore, 'thing:create:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                });

                $scope.$listenTo(ThingStore, 'thing:create:success', function (item) {

                    $scope.state.isUpdatingData = false;

                    var shouldGoToItem = window.confirm("Successfully created, would you like to go to the item update screen?");
                    if (shouldGoToItem === true) {
                        console.info($locationProvider.path);
                        $locationProvider.path('/thing/' + String(item.id));
                    } else {
                        $scope.state.itemTitle = '';
                    }

                    $scope.safeApply();
                });

                $scope.$listenTo(ThingStore, 'thing:create:fail', function (item) {
                    alert("Create failed");
                    $scope.state.isUpdatingData = false;
                });

                this.init();

            }]
        };
    }]);