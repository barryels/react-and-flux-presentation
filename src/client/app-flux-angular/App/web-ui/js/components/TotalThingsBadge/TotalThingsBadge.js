angular.module('App')
    .directive('totalThingsBadge', [function () {
        'use strict';

        return {
            restrict: 'E',
            templateUrl: 'TotalThingsBadge/TotalThingsBadge.html',

            controller: ['$scope', 'flux', 'ThingStore', function ($scope, flux, ThingStore) {

                $scope.state = {
                    total: ThingStore.collection().length
                };

                this.init = function () {
                    $scope.readAll();
                };

                $scope.readAll = function () {
                    flux.dispatch('thing:readAll');
                };

                $scope.safeApply = function () {
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }

                /* Event Listeners */
                $scope.$listenTo(ThingStore, function () {
                    $scope.state.total = ThingStore.collection().length;
                    $scope.safeApply();
                });

                this.init();

            }]
        };
    }]);