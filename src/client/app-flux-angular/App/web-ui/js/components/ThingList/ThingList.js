angular.module('App')
    .directive('thingList', [function () {
        'use strict';

        return {
            restrict: 'E',
            templateUrl: 'ThingList/ThingList.html',

            controller: ['$scope', 'flux', 'ThingStore', '$location', function ($scope, flux, ThingStore, $locationProvider, $render) {

                $scope.props = {
                    totalItemsText_Reading: "(Loading things)",
                    totalItemsText_Creating: "(Creating new thing)",
                    totalItemsText_Success: "Success!",
                    totalItemsText_Fail: "(Last action failed)"
                };

                $scope.state = {
                    collection: [],//ThingStore.collection()
                    totalItemsText: $scope.props.totalItemsText_Reading,
                    isUpdatingData: false
                };

                this.init = function () {
                    $scope.readAll();
                };

                /* Handlers */
                $scope.handleClickDeleteButton = function (itemId) {
                    this.deleteItem(itemId);
                };

                $scope.handleClickViewButton = function (itemId) {
                    $locationProvider.path('/thing/' + itemId);
                };


                /* Action Dsipatchers */
                $scope.readAll = function () {
                    flux.dispatch('thing:readAll');
                };

                $scope.viewItem = function (itemId) {
                    flux.dispatch('thing:view', itemId);
                };

                $scope.deleteItem = function (itemId) {
                    var shouldDelete = window.confirm("Are you sure you wish to delete this item?");
                    if (shouldDelete === true) {
                        flux.dispatch('thing:delete', itemId);
                    } else {
                        // User cancelled the delete operation.
                        // Do nothing, or perhaps display a nice message saying "Thank you for not deleting me :)"
                    }
                };

                $scope.createItem = function (title) {
                    var data = {
                        title: title
                    };
                    flux.dispatch('thing:create', data);
                };

                $scope.safeApply = function () {
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                };

                /* Event Listeners */
                $scope.$listenTo(ThingStore, function () {
                    //$scope.state.isUpdatingData = false;
                    $scope.state.collection = ThingStore.collection();
                    $scope.safeApply();
                });


                $scope.$listenTo(ThingStore, 'thing:readAll:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Reading;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:readAll:success', function (collection) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.collection = ThingStore.collection();
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Success;
                    $scope.safeApply();
                });


                $scope.$listenTo(ThingStore, 'thing:create:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Creating;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:create:success', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Success;
                    //$scope.readAll();
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:create:fail', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Fail;
                    $scope.safeApply();
                });


                $scope.$listenTo(ThingStore, 'thing:delete:progress', function (item) {
                    $scope.state.isUpdatingData = true;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Deleting;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:delete:success', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Success;
                    $scope.safeApply();
                });
                $scope.$listenTo(ThingStore, 'thing:delete:fail', function (item) {
                    $scope.state.isUpdatingData = false;
                    $scope.state.totalItemsText = $scope.props.totalItemsText_Fail;
                    $scope.safeApply();
                });

                this.init();

            }]
        };
    }]);