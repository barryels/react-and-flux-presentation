angular.module('App')
    .store('ThingStore', ['flux', 'Manifest', function (flux, Manifest) {

        'use strict';

        return {

            collection: [],

            handlers: {
                'thing:create': 'create',
                'thing:read': 'read',
                'thing:update': 'update',
                'thing:delete': 'remove',
                'thing:readAll': 'readAll',
                'thing:readLastDeleted': 'readLastDeleted'
            },


            readAll: function () {
                var self = this;

                Manifest.readAllThings()
                    .done(function (response) {
                        self.collection = response;
                        self.emitChange();
                    });
            },

            create: function (data) {
                var self = this;

                this.emit('thing:create:progress');

                data.title = data.title ? data.title : "Placeholder " + Math.round(Math.random() * 10);

                Manifest.createThing(data)
                    .done(function (response) {
                        if (response !== null) {
                            self.emit("thing:create:success", response);
                            self.readAll();
                            //self.emitChange();
                        } else {
                            self.emit("thing:create:fail", response);
                        }
                    });
            },

            read: function (id) {
                var self = this;

                this.emit('thing:read:progress');

                Manifest.readThing(id)
                    .done(function (response) {
                        if (response !== null) {
                            self.emit("thing:read:success", response);
                        } else {
                            self.emit("thing:read:fail", response);
                        }
                    });
            },

            update: function (data) {
                var self = this;

                this.emit('thing:update:progress', data);

                Manifest.updateThing(data)
                    .done(function (response) {
                        if (response !== null) {
                            self.emit("thing:update:success", response);
                            self.readAll();
                        } else {
                            self.emit("thing:update:fail", response);
                        }
                    });
            },

            remove: function (id) {
                var self = this;

                this.emit('thing:delete:progress');

                Manifest.deleteThing(id)
                    .done(function (response) {
                        if (response !== null) {
                            self.emit("thing:delete:success", response);
                            self.readAll();
                        } else {
                            self.emit("thing:delete:fail", response);
                        }
                    });
            },

            exports: {
                collection: function () {
                    return this.collection;
                }
            }

        };
    }]);
