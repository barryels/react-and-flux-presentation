name: main
class: center, middle, bg-dark
.sectionFooter[https://bitbucket.org/barryels/react-and-flux-presentation]
---
name: main-why
class: center, middle, bg-dark
.sectionTitle[Why?]
---
name: main-react
class: center, middle, bg-react
.sectionTitle[What is React?]
---
name: main-flux
class: center, middle, bg-flux
.sectionTitle[What is Flux?]
---
name: code
class: left, middle, bg-dark
---
name: code-react
class: left, middle, bg-react
.sectionTitle[What is React?]
---
name: code-flux
class: left, middle, bg-flux
.sectionTitle[What is Flux?]
---


---
template: main

# React.js <strike>& Flux</strike>

Rethinking Web Development Best Practices

---
template: main

# Presentation Format

- Adhoc Q&A (Ask questions as and when you like, although I'll probably be covering the answer in an upcoming slide)
- ~40 minutes presentation
- ~20 minutes formal Q&A
- Presentation is online: https://bitbucket.org/barryels/react-and-flux-presentation

---
template: main

# Housekeeping

???
- Who is primarily a front-end dev?
- Who's heard of / used React or Flux? (Either in personal or prod capacities)
- About me
- Going to talk about a lot of things
- A lot of these things make sense to me, ask questions if it doesn't make sense to you

---

template: main
# Outline
### What is React?
### What is Flux?
### Putting it all together


---
template: main-react

# What is React?

---
template: main-react

## It's the V in MV*

???

- Unopinionated about your stack
- Isolated to the mounted DOM node

---
template: main-react

## JSX
## Re-render All the Things
## Virtual DOM
## Component-based Architecture
## Synthetic Events
## High Cohesion. Low Coupling
## Isomorphic

---
template: main-react

# JSX

### Completely optional syntactic sugar.

### An XML-like syntax for describing your UI (The word "Describing" is vitally important).

### You are not actually writing this DOM description to the browser.

### React will take this description and shove it into the Virtual DOM algorithm.

---
template: main-react


.left[JSX]

```xml
<h1 id="title">Hello World</h1>
```

__

.left[Vanilla JS]

```javascript
React.createElement("h1", {id: "title"}, "Hello World")
```

---
template: main-react

# Virtual DOM

---
template: main-react

### Diffs the current vs next DOM definitions
### Imperatively updates the existing DOM to accurately represent your DOM declaration
### Minimal, batched updates

???

### An efficient algorithm that diffs the current DOM and the next DOM (after props and state changes) according to your component definition.
### Big O notation: O(n3) vs O(n)
### Imperatively updates the existing DOM to accurately represent your DOM declaration with the newly supplied data.
### Updates are done in batch whenever possible, so as to minimise the number of browser "reflows" that occur.
### It also finds the least number of steps required to go from `DOM A` to `DOM B`

---
template: code-react

```no-highlight
    +---------------+
    |   Your code   |
    +-------+-------+
            |
            +-- Render something like this!
            |
    +-------V-------+           +-------------------+
    |     React     <-----+----->      Browser      |
    +-------+-------+     |     +-------------------+
            |             +- Q: What do you look like?
            |             +- A: These are the DOM nodes you're looking for
            |
    +-------V-------+           +-------------------+
    |     React     <----------->   Diff Algorithm  |
    +-------+-------+           |                   |
            |                   |  DOM now vs next  |
            |                   +-------------------+
            |
            +-- Make these changes!
            |
    +-------V-------+
    |    Browser    |
    +---------------+
```


---
template: code-react

**Current DOM:**
```html
<ul>
    <li id="1"><span>André</span> is <span>online</span></li>
    <li id="2"><span>Barry</span> is <span>offline</span></li>
    <li id="3"><span>Xavier</span> is <span>offline</span></li>
</ul>
```

<br />

**Next state:**
```javascript
users: [
    { "id": "1", name: "André", state: "offline" },
    { "id": "2", name: "Barry", state: "offline" }
    { "id": "3", name: "Prof. Xavier", state: "online" }
]
```

---
template: code-react

**DOM Before:**
```xml
<ul>
    <li id="1"><span>André</span> is <span>online</span></li>
    <li id="2"><span>Barry</span> is <span>offline</span></li>
    <li id="3"><span>Xavier</span> is <span>offline</span></li>
</ul>
```

---
template: code-react

**DOM After:**
```xml
<ul>
    <li id="1"><span>André</span> is <span>offline</span></li>
    <li id="2"><span>Barry</span> is <span>offline</span></li>
    <li id="3"><span>Prof. Xavier</span> is <span>online</span></li>
</ul>

```

---
template: main-react

# Component-based Architecture

## Not a Template.
## A representation of your view
## Pure Javascript

```no-highlight
./src/client/js/views/components/examples/HelloWorld.js
```

???

# Separation of Concerns

## Single Responsibility Principle
"Things that change for the same reasons should be grouped together."
"Things that change for different reasons should be separated."

## Coupling vs Cohesion

### Coupling:
The degree to which each module relies on each of the other modules.

### Cohesion:
The degree to which elements of a module belong together.

Separation of concerns is not about separating technologies. It's about separating things that should not change together.

A module's display logic and its markup are tightly coupled.

---
template: main-react

# Synthetic Events

React dynamically overrides browser events with its own SyntheticEvent class.
E.g. onClick, onSubmit, onChange, etc.
It binds these events to the root mount node.

---
template: main-react

# React in the Wild

- Facebook chat client
- Facebook Ads Management Platform
- All of Instagram.com
- Atlassian HipChat
- Yahoo Mail client (WIP)
- Atom.io (WIP)
- Adobe Brackets - Project tree component
- [And lots more](https://github.com/facebook/react/wiki/Sites-Using-React)


---
template: main-flux
# What is Flux?

---
template: main-flux
## Design Pattern
It's a design pattern, not a library.

---
template: main-flux
## Principles
- Unidirectional Data Flow
- Stores only have getters (no setters)
- Events are the foundation for communication

???

- Ward Cunningham once said, about "What is clean code?"
    "Clean code is when you look at a routine and it's pretty much what you expected"
    When was the last time that you looked at a function, that, either you, or someone else, wrote and you thought: WTF?
- On of the best metrics for clean code is WTFs per minute, 
    what Ward meant was that clean code has a metric of 0 WTFs per minute.

---
template: main-flux
## What is it trying to solve?

---
template: main-flux
## Modern MVC

<img src="./../assets/img/presentation/family-guy-mvc.gif" width="600" />

---
template: main-flux

**1** is simple. **1n** is complex, right?

---
template: code-flux

```no-highlight
                                      1n
                                       
                                       |
                                       |
                                       |
                                       V
```

---
template: code-flux

```no-highlight
                                1n (where n = 9)

                               | | | | | | | | |
                               | | | | | | | | |
                               | | | | | | | | |
                               V V V V V V V V V
```

---
template: code-flux

```no-highlight
                                1n (where n = 17)

                       | | | | | | | | | | | | | | | | |
                       | | | | | | | | | | | | | | | | |
                       | | | | | | | | | | | | | | | | |
                       V V V V V V V V V V V V V V V V V
```

---
template: code-flux

What happens when **n = fn () {}**

```no-highlight
                        1n (where n = (a) {
                            4a - 3 + 2a$scope - 1 ? true
                            !$scope.invalid ? 
                                2a - 5a * 2 ? true : 
                                    5a ? false : null
                                : return n(a * 2)
                            return false
                        })

                       A A | A | | | A | | A | | A A | | 
                       \ / | | | | | | / | | | | \ | / +----->
                        |  | | | | | |/  | | | |  \|/  
                        V  V V V V V V   V V V V   V   
```

---
template: main-flux

### Complexity comes **not** from **the number of things** in a system, but rather from their **deviation from a standard pattern**.

---
template: code-flux

```no-highlight
        +------------------+
    +--->      Action      |
    |   +--------+---------+
    |            |          
    |            |          
    |   +--------V---------+
    |   |    Dispatcher    |
    |   +--------+---------+
    |            |          
    |            |          
    |   +--------V---------+    +-----------------+
    |   |      Store       <---->     Web API     |
    |   +--------+---------+    +-----------------+
    |            |          
    |            |          
    |   +--------V---------+
    +---+    Component     |
        +------------------+
```    

---
template: main-flux
## Action

The public API for your application

It's a "use case"

---
template: main-flux
## Dispatcher
Semi-intelligent Pub/Sub Eventing System

---
template: main-flux
## Store

Contains data that represents a logical domain.

The data can be anything: models, collections, whatever.

Only has getters, no setters.

---
template: main-flux
## Component

Is concerned only with its own **props** and **state**.

Only knows about the outside world through **Actions** and **Stores**.

Calls **Action** methods when it wants to change app-wide state.

Subscribes to **Store** events in order to know when / whether those state changes have taken place.

---
template: main-flux

## Libraries
Again, Flux is a Design Pattern, not a library.

Many libraries have come out that address some of its shortcomings, 

For example:

- Nasty switch statements in Stores, and
- Manually binding / unbinding event listeners in Components




---
template: main
# Putting it all together

### A task management app

---
template: main
# Thank You

## Questions?

---
template: main

---
template: main

# Give it 5 minutes
Don't think about frameworks or libraries.
Forget about implementation concerns.
Simply think about the problem you're trying to solve in your domain.

---
template: main

# But the DOM is slow
In practice it diffs the DOM tree and does the minimal set of updates required.


---
template: main
# Third Party Library support (e.g. jQuery, et. al.)

---
template: main
# Testing

???

- I've only ever needed to test my functions, never DOM code
- The DOM is just a delivery mechanism,
- If it "doesn't work" in a particular browser, then we'll look into it, but really, we shouldn't be bothered by 
misbehaving browsers, it leads us down a long solemn path
- If the unit tests pass, then the code logic is sound, browser testing is another talk for another time

---
template: main
## Influencers

- **CQRS** (Command Query Responsibility Segregation)

[Reference](http://martinfowler.com/bliki/CQRS.html)

---
template: main-why

# Why use Frameworks / Libraries at all?

- Consistent, reliable user interfaces are difficult to build.
- Abstractions are good when they allow you to write your domain logic without worrying too much about raw implementation details.
- Reducing the complexity of user interface development is vitally important to ensuring both consistency and reliability.


---
template: main
# NOTES
- Once someone learns what React is and how to use it, they lose the ability to explain it to other people.
- Just the View, Virtual DOM, Data Flow
- Props & State
- Props are the data that a child gets from its parent
- State is the data that a child creates and can mutate
- Thinking in Components
- What is a UI actually?
    - Just a different view of our data (which is stored on a disk on a server somewhere)
- What's the point of a View library anyway?
    - A view library should let us change data and transparently keep the UI in sync.
    - It should allow us to structure the UI into components, and
    - flow data between them sanely.
- [Stars](https://github.com/showcases/front-end-javascript-frameworks?s=stars)
- Quotes
There is no standard and universal way to define mixins in JavaScript. In fact, several features to support mixins were dropped from ES6 today. There are a lot of libraries with different semantics. We think that there should be one way of defining mixins that you can use for any JavaScript class. React just making another doesn’t help that effort.

---
template: main
# TODO
- [?] Add "Back" button component
- [ ] Task: Get all [X], Create [X], Read [ ], Update [X], Delete [X]
- [ ] User: Get all [X], Create [X], Read [ ], Update [ ], Delete [ ]
- [ ] User: Assign to task
- [ ] Task: Validate title (must contain "#1234 string")
- [ ] User: Validate props (firstname & lastname & email address)
- [ ] Notification: Error - user with email address already exists

---
template: main
# TODONE
- [X] Remove extraneous events, etc.
- [X] #presentation - Move this into a jade / plain HTML template to allow for page-based viewing
    - [X] Use remark slide generator
- [X] Move each example into its own page
- [X] Task: Filter list by title
- [X] User: Filter list by fullname


