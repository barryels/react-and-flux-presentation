(function () {
    'use strict';
    
    var Dispatcher = require('../../core/Dispatcher');
    var TaskConstants = require('./Constants');
    
    module.exports = {

        readAll: function (payload) {
            Dispatcher.dispatch({"action": TaskConstants.TASK_READALL});
        },

        createTask: function (payload) {
            Dispatcher.dispatch({"action": TaskConstants.TASK_CREATE, "payload": payload});
        },

        updateTask: function (payload) {
            Dispatcher.dispatch({"action": TaskConstants.TASK_UPDATE, "payload": payload});
        },

        deleteTask: function (payload) {
            Dispatcher.dispatch({"action": TaskConstants.TASK_DELETE, "payload": payload});
        }
        
        
    };

}());
