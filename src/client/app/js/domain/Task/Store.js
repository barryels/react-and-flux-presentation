(function () {
    'use strict';

    var _ = require('lodash');
    var EventEmitter = require('wolfy87-eventemitter');
    var Dispatcher = require('../../core/Dispatcher');
    var Manifest = require('../../core/Manifest');

    var Constants = require('./Constants');
    var Actions = require('./Actions');

    var Store = new EventEmitter();

    var _taskcollection = [];

    Manifest.registerEndpoint("getAllTasks", "//localhost:3006/api/0.1/tasks", "GET");
    Manifest.registerEndpoint("getTask", "//localhost:3006/api/0.1/tasks", "GET", {
        "id": "string"
    });
    Manifest.registerEndpoint("createTask", "//localhost:3006/api/0.1/tasks", "POST", {
        "title": "string"
    });
    Manifest.registerEndpoint("updateTask", "//localhost:3006/api/0.1/tasks", "PUT");
    Manifest.registerEndpoint("deleteTask", "//localhost:3006/api/0.1/tasks", "DELETE");

    var dispatchToken = Dispatcher.register(function (e) {

        //console.log("TaskStore() received action:", e);

        switch (e.action) {
            case Constants.TASK_READALL:
                handleReadAll(e.payload);
                break;

            case Constants.TASK_READ:
                handleRead(e.payload);
                break;

            case Constants.TASK_CREATE:
                handleCreate(e.payload);
                break;
        }

    });

    var handleReadAll = function (payload) {
        Manifest.callEndpoint("getAllTasks", payload)
            .then(function (response) {
                _taskcollection = response.payload;
                Store.emit(Constants.TASK_CHANGE);
            })
            .fail(function (response) {

            });
    };

    var handleCreate = function (payload) {

        Store.emit(Constants.TASK_CREATE_START);

        Manifest.callEndpoint("createTask", payload, 1500)
            .then(function (response) {
                _taskcollection.push(response.payload);
                Store.emit(Constants.TASK_CREATE_RESPONSE);
                Store.emit(Constants.TASK_CREATE_SUCCESS);
                Store.emit(Constants.TASK_CHANGE);
            })
            .fail(function (response) {

            });
    };


    Store.all = function () {
        return _taskcollection;
    };

    Store.one = function (data) {
        var result = {};

        if (typeof data === "string" || typeof data === "number") {
            console.info(_taskcollection, data);
            result = _.find(_taskcollection, function (item) {
                return item._id === data;
            });
        } else {
            result = _.find(_taskcollection, data);
        }

        return result;
    };
    
    module.exports = Store;

}());