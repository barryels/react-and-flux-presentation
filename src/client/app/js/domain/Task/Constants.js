/**
 * Created by barryels on 2015/04/04.
 */
(function () {

    'use strict';
    
    var Crudify = require('../../core/Crudify');
    
    var _constants = Crudify.keymirror({
        TASK_CHANGE: "",
        TASK_READALL: "",

        TASK_CREATE: "",
        TASK_CREATE_START: "",
        TASK_CREATE_RESPONSE: "",
        TASK_CREATE_FAIL: "",
        TASK_CREATE_SUCCESS: "",

        TASK_UPDATE: "",
        TASK_READ: "",
        TASK_DELETE: ""
    });

    module.exports = _constants;

}());