(function () {
    'use strict';

    var Constants = require('./User/Constants');
    var Actions = require('./User/Actions');
    var Store = require('./User/Store');
    
    module.exports = {
        constants: Constants,
        actions: Actions,
        store: Store
    };

}());
