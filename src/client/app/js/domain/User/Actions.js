(function () {
    'use strict';
    
    var Dispatcher = require('../../core/Dispatcher');
    var UserConstants = require('./Constants');
    
    module.exports = {

        getAllUsers: function (payload) {
            Dispatcher.dispatch({"action": UserConstants.USER_READALL});
        },

        createUser: function (payload) {
            Dispatcher.dispatch({"action": UserConstants.USER_CREATE, "payload": payload});
        },

        updateUser: function (payload) {
            Dispatcher.dispatch({"action": UserConstants.USER_UPDATE, "payload": payload});
        },

        deleteUser: function (payload) {
            Dispatcher.dispatch({"action": UserConstants.USER_DELETE, "payload": payload});
        },

        assignUserToTask: function (payload) {
            Dispatcher.dispatch({"action": UserConstants.USER_ASSIGN_TO_TASK, "payload": payload});
        }
    };

}());
