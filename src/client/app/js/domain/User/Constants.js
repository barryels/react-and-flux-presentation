/**
 * Created by barryels on 2015/04/04.
 */
(function () {

    'use strict';

    
    var Crudify = require('../../core/Crudify');
    
    var _constants = Crudify.keymirror({
        USER_CHANGE: "",

        USER_READALL: "",
        USER_READALL_SUCCESS: "",
        USER_READALL_FAIL: "",

        USER_CREATE: "",
        USER_CREATE_SUCCESS: "",
        USER_CREATE_FAIL: "",

        USER_READ: "",
        USER_READ_SUCCESS: "",
        USER_READ_FAIL: "",

        USER_UPDATE: "",
        USER_UPDATE_SUCCESS: "",
        USER_UPDATE_FAIL: "",

        USER_ASSIGN_TO_TASK: "",

        USER_DELETE: "",
        USER_DELETE_SUCCESS: "",
        USER_DELETE_FAIL: ""
    });

    module.exports = _constants;

}());