(function () {
    'use strict';

    var _ = require('lodash');
    var EventEmitter = require('wolfy87-eventemitter');
    var Dispatcher = require('../../core/Dispatcher');
    var Manifest = require('../../core/Manifest');

    var Constants = require('./Constants');
    var Actions = require('./Actions');

    var Store = new EventEmitter();

    var _usercollection = [];

    Manifest.registerEndpoint("getAllUsers", "//localhost:3006/api/0.1/users", "GET");
    Manifest.registerEndpoint("getUser", "//localhost:3006/api/0.1/users", "GET", {
        "id": "string"
    });
    Manifest.registerEndpoint("createUser", "//localhost:3006/api/0.1/users", "POST", {
        "firstName": "string",
        "lastName": "string"
    });
    Manifest.registerEndpoint("updateUser", "//localhost:3006/api/0.1/users", "PUT");
    Manifest.registerEndpoint("deleteUser", "//localhost:3006/api/0.1/users", "DELETE");

    var dispatchToken = Dispatcher.register(function (e) {

        //console.log("UserStore() received action:", e);

        switch (e.action) {
            case Constants.USER_READALL:
                handleReadAll(e.payload);
                break;

            case Constants.USER_CREATE:
                console.info(e);
                handleCreate(e.payload);
                break;

            case Constants.USER_UPDATE:
                handleUpdate(e.payload);
                break;

            case Constants.USER_DELETE:
                handleDelete(e.payload);
                break;
        }

    });

    var handleReadAll = function (payload) {
        Manifest.callEndpoint("getAllUsers", payload)
            .then(function (response) {
                _usercollection = response.payload;

                Store.emit(Constants.USER_CHANGE);
            })
            .fail(function (response) {

            });
    };

    var handleCreate = function (payload) {
        Manifest.callEndpoint("createUser", payload)
            .then(function (response) {

                _usercollection.push(response.payload);

                Store.emit(Constants.USER_CHANGE);
            })
            .fail(function (response) {

            });
    };

    var handleUpdate = function (payload) {
        var _updatedItem;

        Manifest.callEndpoint("updateUser", payload)
            .then(function (response) {
    
                _updatedItem = _.find(_usercollection, function (item) {
                    return item._id === response.payload._id;
                });
    
                if (_updatedItem) {
                    _updatedItem = _.merge(_updatedItem, response.payload);
                }
    
                Store.emit(Constants.USER_CHANGE);
            })
            .fail(function (response) {
    
            });
    };


    var handleDelete = function (payload) {
        console.log(payload);
        if (payload._id) {
            payload.id = payload._id;
            delete payload._id;
        }
        Manifest.callEndpoint("deleteUser", payload)
            .then(function (response) {
                _usercollection = response.payload;

                Store.emit(Constants.USER_CHANGE);
            })
            .fail(function (response) {

            });
    };

    Store.all = function () {
        return _usercollection;
    };

    Store.one = function (data) {
        var result = {};

        if (typeof data === "string" || typeof data === "number") {
            console.info(_usercollection, data);
            result = _.find(_usercollection, function (item) {
                return item._id === data;
            });
        } else {
            result = _.find(_usercollection, data);
        }
        
        return result;
    };
    
    module.exports = Store;

}());