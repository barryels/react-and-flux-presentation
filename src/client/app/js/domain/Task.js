(function () {
    'use strict';

    var Constants = require('./Task/Constants');
    var Actions = require('./Task/Actions');
    var Store = require('./Task/Store');
    
    module.exports = {
        constants: Constants,
        actions: Actions,
        store: Store
    };

}());
