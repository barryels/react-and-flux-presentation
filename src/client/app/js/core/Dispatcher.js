/**
 * Created by barryels on 2015/04/04.
 */
(function () {

    'use strict';

    var flux = require("flux");

    module.exports = new flux.Dispatcher();

}());