/**
 * Created by barryels on 2015/04/04.
 */

jest.autoMockOff();

describe('Manifest', function() {

    it('should add an endpoint', function() {
        var Manifest = require('../index');
        expect(Manifest.endpoints().length).toBe(0);
        Manifest.registerEndpoint("test1", "/url", "GET");
        expect(Manifest.endpoints().length).toBe(1);
    });
    
    it('should generate a unique endpoint signature', function() {
        var Manifest = require('../index');
        var endpoint1 = Manifest.registerEndpoint("endpoint1", "/url", "GET");
        expect(endpoint1.signature).toBe("/url@GET");
        var endpoint2 = Manifest.registerEndpoint("endpoint2", "/url", "GET", {"id":"string"});
        expect(endpoint2.signature).toBe("/url@GET&id=string");
    });

    it('should format the HTTP request type string', function() {
        var Manifest = require('../index');
        var endpoint1 = Manifest.registerEndpoint("endpoint1", "/url", "gEt");
        expect(endpoint1.type).toBe("GET");
    });

    it('should default to "GET" for unknown request types', function() {
        var Manifest = require('../index');
        var endpoint1 = Manifest.registerEndpoint("endpoint1", "/url", "Unsupported Type");
        expect(endpoint1.type).toBe("GET");
    });

    it('should not add an endpoint with the same url signature more than once', function() {
        var Manifest = require('../index');
        Manifest.registerEndpoint("endpoint1", "/url", "GET");
        Manifest.registerEndpoint("endpoint2", "/url", "GET");
        expect(Manifest.endpoints().length).toBe(1);

        Manifest.registerEndpoint("endpoint1", "/url", "POST");
        Manifest.registerEndpoint("endpoint2", "/url", "POST", { id: "id" });
        expect(Manifest.endpoints().length).toBe(2);
    });

    it('should not add an endpoint with the same name more than once', function() {
        var Manifest = require('../index');
        Manifest.registerEndpoint("sameName", "/urlA", "GET");
        Manifest.registerEndpoint("sameName", "/urlB", "POST");
        expect(Manifest.endpoints().length).toBe(1);
    });

    it('should return an endpoint by name', function() {
        var Manifest = require('../index');
        Manifest.registerEndpoint("test1", "/urlA", "GET");

        var endpoint = Manifest.getEndpoint("test1");
        expect(endpoint.name).toBe("test1");
    });

    it('should return an endpoint by supplied object', function() {
        var Manifest = require('../index');
        var endpointData = {
            "name": "test1",
            "url": "/urlA",
            "type": "GET"
        };
        Manifest.registerEndpoint(endpointData.name, endpointData.url, endpointData.type);
        
        expect(Manifest.getEndpoint(endpointData).name).toBe("test1");
        expect(Manifest.getEndpoint({})).toBe(undefined);
    });

    it('should call a mock method', function() {
        var Manifest = require('../index');
        Manifest.registerEndpoint("createUser", "/api/0.1/users", "POST", null, {
            fn: function () {
                return "asdf";
            }
        });
        var result = Manifest.callEndpoint("createUser", {firstName:"Barry", lastName:"Els"}, true);
        //console.warn(typeof(result));
        //expect().toBeCalled();
        //expect(Manifest.callEndpoint).toBeCalledWith(arg, um, ents);
    });
    
    xit('should return a promise', function() {
        var Manifest = require('../index');
        Manifest.registerEndpoint("createUser", "/api/0.1/users", "POST");
        var result = Manifest.callEndpoint("createUser", {firstName:"Barry", lastName:"Els"});
        //console.warn(typeof(result));
        //expect().toBeCalled();
        //expect(Manifest.callEndpoint).toBeCalledWith(arg, um, ents);
    });

    // TODO ? should be able to deregister all endpoints
    // TODO ? should be able to deregister a single endpoint
});