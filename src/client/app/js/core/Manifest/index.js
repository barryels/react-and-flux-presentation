(function () {
    'use strict';

    var _ = require("lodash");
    var Q = require("q");
    var qwest = require("qwest");
    
    /*
     * This object abstracts away external data requests, 
     * so that the UI can be built and tested without worrying about another system being accessible.
     * 
     * Some cool things it can do:
     * 1. Use a mock API for local testing
     * 2. Simulate server response delays through the getDelayDuration() method
     * 3. Mutate data from an external API into a UI compatible data format (Think XML parsing, JSON formatting, etc.)
     * 4. Mutate date sent from the UI into an API compatible structure.
     * TODO
     * [ ] Add offline support
     * [ ] Simulate HTTP response headers
     * [ ] Implement a caching strategy
     *     [ ] Should have a caching strategy selection in the registerEndpoint method
     *         [ ] Should implement a cache based on minimum time between requests
     *     [ ] Should allow an explicit opt-out in the callEndpoint method
     */
    
    var _endpoints = [],
        _httpRequestTypes = {
            "_default": "GET",
            "GET": "GET",
            "POST": "POST",
            "PUT": "PUT",
            "DELETE": "DELETE"
        };

    var _getEndpoint = function (endpointData) {
        var _endpoint;
        
        if (typeof(endpointData) === "string") {
            _endpoint = _.find(_endpoints, function(item) {
                return item.name === endpointData;
            });
        } else {
            _endpoint = _.find(_endpoints, function(item) {
                return item.name === endpointData.name;
            });

            if (_endpoint === undefined) {
                _endpoint = _.find(_endpoints, function (item) {
                    return item.signature === _generateEndpointSignature(endpointData.name, endpointData.url, endpointData.type, endpointData.requestBlueprint);
                });
            }
        }
        
        return _endpoint;
    };

    var _convertHTTPRequestType = function (type) {

        if (type === undefined) {
            type = _httpRequestTypes._default;
        } else {
            type = _httpRequestTypes[type.toUpperCase()];
            if (type === undefined) {
                type = _httpRequestTypes._default;
            }
        }

        return type;
    };

    var _convertHTTPRequestTypeToFunctionName = function (type) {
        var functionName = _convertHTTPRequestType(type).toLowerCase();
        return functionName;
    };

    var _generateEndpointSignature = function (name, url, type, requestBlueprint) {
        var signature = url;
        signature += "@" + _convertHTTPRequestType(type);
        _.each(requestBlueprint, function (value, key) {
            signature += "&" + key +"=" + value;
        });

        return signature;
    };

    var _getDelayDuration = function () {
        return 0; // Fastest 0s

        //return (Math.floor(Math.random() * 100) + 100); // Faster: > 0.1s < 0.2s // Expected result
        //return (Math.floor(Math.random() * 250) + 250); // Fast: > 0.25s < 0.5s
        //return (Math.floor(Math.random() * 500) + 500); // Slow: > 0.5s < 1s
        //return (Math.floor(Math.random() * 1000) + 1000); // Slower: > 1s < 2s
        //return (Math.floor(Math.random() * 5000) + 5000); // Slowest: > 5s < 10s
    };

    var _createEndpointRequest = function (endpoint, payload) {
        var request = _.cloneDeep(endpoint);

        request.data = _.cloneDeep(payload);

        if (request.headers === undefined) {
            request.headers = {
                "Accept": "application/json"
            };
        }

        if (request.dataType === undefined) {
            request.dataType = "json";
        }

        return request;
    };
    
    var _apiCall = function (request, delayDuration) {
        //console.info("_apiCall()", request);
        var defer = Q.defer(),
            _request = request,
            ajax = {},
            _delayDuration;

        if (delayDuration) {
            _delayDuration = delayDuration;
        } else {
            _delayDuration = _getDelayDuration();
        }

        //console.log(_request);

        _.delay(function (ajax, defer) {
            ajax = qwest[_convertHTTPRequestTypeToFunctionName(_request.type)](_request.url, _request.data)
                .then(function (response) {
                    defer.resolve(response);
                })
                .catch(function (e, url) {
                    // Process the error
                    console.warn("error", e);
                    console.warn("StackTrace", e.stack);
                });
        }, _delayDuration, ajax, defer);
        
        return defer.promise;
    };


    module.exports = {

        endpoints: function () {
            return _endpoints.slice();
        },

        registerEndpoint: function (name, url, type, requestBlueprint, mockData) {

            var signature = _generateEndpointSignature(name, url, type, requestBlueprint);
            var _type = _convertHTTPRequestType(type);

            var newEndpoint = {
                name: name,
                url: url,
                type: _type,
                signature: signature,
                requestBlueprint: requestBlueprint,
                mockData: mockData
            };

            var existingEndpoint = _getEndpoint(newEndpoint);

            if (existingEndpoint !== undefined) {
                if (existingEndpoint.name === name) {
                    //console.warn('Manifest.js', 'An endpoint with name: "' + name + '" already exists');
                } else {
                    //console.warn('Manifest.js', 'An endpoint with signature: "' + signature + '" already exists');
                }
                return existingEndpoint;
            } else {
                //console.info('Manifest.js', 'Endpoint with signature: "' + signature + '" successfully registered on: Manifest.callEndpoint("' + name + '")');
                _endpoints.push(newEndpoint);
                return existingEndpoint ? existingEndpoint : _endpoints[_endpoints.length - 1];
            }
            
        },
        
        getEndpoint: _getEndpoint,

        callEndpoint: function (name, requestPayload, delayDuration) {
            var endpoint = _getEndpoint(name);
            var endpointRequest = _createEndpointRequest(endpoint, requestPayload);
            
            return _apiCall(endpointRequest, delayDuration);
        }

    };

}());
