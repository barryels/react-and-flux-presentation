'use strict';

var React = require('react');
var Router = require('react-router');

var LayoutDefault = require('../views/layouts/Default');

var PageIndex = require('../views/pages/Index');

var PageExamples = require('../views/pages/examples/Index');
var Page1 = require('../views/pages/examples/Page1');
var Page2 = require('../views/pages/examples/Page2');
var Page3 = require('../views/pages/examples/Page3');
var Page4 = require('../views/pages/examples/Page4');
var Page5 = require('../views/pages/examples/Page5');

var PageHome = require('../views/pages/taskr/Home');
var PageLogin = require('../views/pages/taskr/Login');
var PageUsers = require('../views/pages/taskr/Users');
var PageTasks = require('../views/pages/taskr/Tasks');

var routes = (
    <Router.Route name="Home" handler={LayoutDefault} path="/">
        <Router.Route name="index" path="/" handler={PageIndex}/>

        <Router.Route name="examples" path="/examples" handler={PageExamples}/>
        <Router.Route name="examples-page1" path="/examples/1" handler={Page1}/>
        <Router.Route name="examples-page2" path="/examples/2" handler={Page2}/>
        <Router.Route name="examples-page3" path="/examples/3" handler={Page3}/>
        <Router.Route name="examples-page4" path="/examples/4" handler={Page4}/>
        <Router.Route name="examples-page5" path="/examples/5" handler={Page5}/>

        <Router.Route name="home" path="/home" handler={PageHome}/>
        <Router.Route name="login" path="/login" handler={PageLogin}/>
        <Router.Route name="users" path="/users" handler={PageUsers}/>
        <Router.Route name="tasks" path="/tasks" handler={PageTasks}/>
    </Router.Route>
);

module.exports = {
    init: function () {
        Router.run(routes, function (Handler) {
            React.render(<Handler />, document.body);
        });
    }
};
