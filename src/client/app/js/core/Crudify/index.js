/**
 * Created by barryels on 2015/04/04.
 */
(function () {

    'use strict';
    
    var _ = require('lodash');
    
    var _keymirror = function (source, prefix) {
        var result = {};
        if (!prefix) {
            prefix = "";
        }
        _.forEach(source, function (val, key) {
            result[key] = prefix + key;
        });
        return result;
    };

    var _expand = function (sources, handlers, events) {
        var result = {};

        _.forEach(sources, function (sourceValue, sourceKey) {
            var sourceName = sourceValue;
            
            if (handlers === undefined || handlers.length === 0) {
                result[sourceName] = sourceName;
            } else {
                _.forEach(handlers, function (handlerValue, handlerKey) {
                    var handlerName = sources + "_" + handlerValue;
                    result[handlerName] = handlerName;
    
                    if (events !== undefined && events.length !== 0) {
                        _.forEach(events, function (eventValue, eventKey) {
                            var eventName = handlerName + "_" + eventValue;
                            result[eventName] = eventName;
                        });
                    }
                });
            }
        });
        
        
        return result;
    };
    
    module.exports = {
        expand: _expand,
        keymirror: _keymirror
    };

}());