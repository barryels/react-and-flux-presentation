/**
 * Created by barryels on 2015/04/04.
 */

jest.autoMockOff();

describe('Crudify', function() {

    it('should set the value to the key', function() {
        var Crudify = require('../index');

        var testObject1 = Crudify.keymirror({"USER": ""});
        expect(testObject1).toEqual({
            "USER": "USER"
        });
    });

    it('should set the value to the key including a prefix', function() {
        var Crudify = require('../index');

        var testObject1 = Crudify.keymirror({"ACTION": ""}, "USER_");
        expect(testObject1).toEqual({
            "ACTION": "USER_ACTION"
        });
    });

    it('should simply set the value to the key when no handlers are supplied', function() {
        var Crudify = require('../index');

        var testObject1 = Crudify.expand(["USER"]);
        expect(testObject1).toEqual({
            "USER": "USER"
        });

        var testObject2 = Crudify.expand(["USER"], [], []);
        expect(testObject2).toEqual({
            "USER": "USER"
        });

        var testObject3 = Crudify.expand(["THING1", "THING2", "THING3"]);
        expect(testObject3).toEqual({
            "THING1": "THING1",
            "THING2": "THING2",
            "THING3": "THING3"
        });
    });

    it('should expand an object with CRUD names', function() {
        var Crudify = require('../index');
        
        var testObject = Crudify.expand(["USER"], ["READALL", "CREATE", "READ", "UPDATE", "DELETE"]);
        expect(testObject).toEqual({
            "USER_READALL": "USER_READALL",
            "USER_CREATE": "USER_CREATE",
            "USER_READ": "USER_READ",
            "USER_UPDATE": "USER_UPDATE",
            "USER_DELETE": "USER_DELETE"
        });
    });

    it('should expand an object with CRUD names plus events', function() {
        var Crudify = require('../index');
        
        var testObject = Crudify.expand(["USER"], ["READALL", "CREATE"], ["RESPONSE", "SUCCESS", "FAIL"]);
        expect(testObject).toEqual({
            "USER_READALL": "USER_READALL",
            "USER_READALL_RESPONSE": "USER_READALL_RESPONSE",
            "USER_READALL_SUCCESS": "USER_READALL_SUCCESS",
            "USER_READALL_FAIL": "USER_READALL_FAIL",
            "USER_CREATE": "USER_CREATE",
            "USER_CREATE_RESPONSE": "USER_CREATE_RESPONSE",
            "USER_CREATE_SUCCESS": "USER_CREATE_SUCCESS",
            "USER_CREATE_FAIL": "USER_CREATE_FAIL"
        });
    });

});