'use strict';

var React = require('react');

var HelloWorld = React.createClass({

    getInitialState: function () {
        return {
            text: "asdf"
        }
    },

    onInputChange: function (e) {
        this.setState({
            text: e.target.value
        });
    },

    filterToUpperCase: function (input) {
        var output = input.toUpperCase();

        return output;
    },

    render: function () {

        return (
            <div>
                <input type="text" onChange={this.onInputChange} value={this.state.text}/>
                <span>Hello {this.filterToUpperCase(this.state.text)}</span>
            </div>
        );
    }

});

module.exports = HelloWorld;
