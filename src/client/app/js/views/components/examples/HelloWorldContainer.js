'use strict';

var React = require('react');

var Input = require('./HelloWorldContainer/Input');
var Output = require('./HelloWorldContainer/Output');

module.exports = React.createClass({

    getInitialState: function () {
        return {
            name: ""
        };
    },

    handleChangeName: function (name) {
        this.setState({
            name: name
        });
    },

    render: function () {
        return (
            <div>
                <Input name={this.state.name} handleChangeName={this.handleChangeName}/>
                <Output name={this.state.name}/>
                <Output name={this.state.name} format="uppercase"/>
                <Output name={this.state.name} format="reverse"/>
            </div>
        );
    }

});

