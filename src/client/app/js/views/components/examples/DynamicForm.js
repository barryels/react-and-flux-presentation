'use strict';

var React = require('react');

var FormControls = {};

FormControls.TextInput = React.createClass({
    propTypes: {
        placeholder: React.PropTypes.string.isRequired,
        name: React.PropTypes.string.isRequired
    },
    getInitialState: function () {
        return {
            value: this.props.value
        };
    },
    handleValueChange: function (e) {
        this.setState({
            value: e.target.value
        });
    },
    render: function () {
        return (
            <div className="form-group">
                <input type="text" className="form-control" name={this.props.name} onChange={this.handleValueChange}
                       value={this.state.value} placeholder={this.props.placeholder}/>
            </div>
        )
    }
});


FormControls.BasicButton = React.createClass({
    propTypes: {
        label: React.PropTypes.string.isRequired
    },
    handleClick: function (e) {
        if (this.props.handleClick) {
            this.props.handleClick(e);
        }
    },
    render: function () {
        return (
            <div className="form-group">
                <button className="btn btn-primary">{this.props.label}</button>
            </div>
        )
    }
});


var DynamicForm = React.createClass({

    getDefaultProps: function () {
        return {
            inputTextValue: "",
            formSchema: {
                items: [
                    {
                        type: FormControls.TextInput,
                        props: {
                            name: "firstName",
                            value: "asdf"
                        }
                    },
                    {
                        type: FormControls.TextInput,
                        props: {
                            name: "lastName",
                            value: "",
                            placeholder: "Last Name"
                        }
                    },
                    {
                        type: FormControls.BasicButton,
                        props: {
                            label: "Button 1"
                        }
                    }

                ]
            }
        };
    },

    handleSubmit: function (e) {
        e.preventDefault();
        console.log("handleSubmit()", this.refs.form);
    },

    render: function () {
        var i = 0;
        return (

            <form ref="form" onSubmit={this.handleSubmit}>

                {this.props.formSchema.items.map(function (itemSchema) {
                    var itemComponent = FormControls[itemSchema.type];
                    i++;
                    return (
                        <itemSchema.type key={i} {...itemSchema.props}></itemSchema.type>
                    )
                })}

            </form>
        );
    }

});

module.exports = DynamicForm;
