/**
 * Created by barryels on 2015/04/09.
 */

(function () {
    'use strict';

    var React = require('react');

    var DragAndDrop = React.createClass({

        getDefaultProps: function () {
            return {
                name: "",
                relative: {
                    x: 0,
                    y: 0
                },
                absolute: {
                    x: 0,
                    y: 0
                },
                zIndex: 1
            };
        },

        getInitialState: function () {
            return {
                isDragging: false,
                relative: this.props.relative,
                absolute: this.props.absolute
            };
        },

        stopDragging: function () {
            this.setState({
                isDragging: false,
                zIndex: this.props.zIndex
            });
        },

        handleMouseDown: function (e) {
            this.setState({
                isDragging: true,
                relative: {
                    x: e.pageX - this.state.absolute.x,
                    y: e.pageY - this.state.absolute.y
                },
                zIndex: 1000
            });
        },

        handleMouseUp: function (e) {
            this.stopDragging();
        },

        handleMouseLeave: function (e) {
            this.stopDragging();
        },

        handleMouseMove: function (e) {
            this.setState({
                absolute: {
                    x: e.pageX - this.state.relative.x,
                    y: e.pageY - this.state.relative.y
                }
            });
        },

        render: function () {
            var handleMouseDown = this.handleMouseDown;
            var handleMouseUp = this.handleMouseUp;
            var handleMouseLeave = this.handleMouseLeave;
            var handleMouseMove = this.handleMouseMove;

            if (this.state.isDragging) {
                handleMouseDown = null;
            } else {
                handleMouseUp = null;
                handleMouseLeave = null;
                handleMouseMove = null;
            }

            var style = {
                "position": "absolute",
                "padding": "20px",
                "border": "1px solid #000",
                "cursor": "pointer",
                "background": "#eee",
                "WebkitTouchCallout": "none",
                "WebkitUserSelect": "none",
                "KhtmlUserSelect": "none",
                "-moz-user-select": "none",
                "-ms-user-select": "none",
                "user-select": "none",
                "left": this.state.absolute.x +"px",
                "top": this.state.absolute.y +"px",
                "zIndex": this.state.zIndex
            };

            return (
                <div style={style} onMouseDown={handleMouseDown} onMouseUp={handleMouseUp} onMouseLeave={handleMouseLeave} onMouseMove={handleMouseMove}>
                    {this.props.name}
                </div>
            );
        }
    });

    module.exports = DragAndDrop;

}());
