/**
 * Created by barryels on 2015/04/03.
 */

(function () {
    'use strict';

    var React = require('react');
    
    module.exports = React.createClass({

        handleChangeName: function (e) {
            this.props.handleChangeName(e.target.value);
        },
        
        render: function () {
            return (
                <div>
                    <input type="text" onChange={this.handleChangeName} value={this.props.name} />
                </div>
            );
        }
    
    });

}());