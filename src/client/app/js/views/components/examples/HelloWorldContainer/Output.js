/**
 * Created by barryels on 2015/04/03.
 */

(function () {
    'use strict';

    var React = require('react');
    var TextFormat = require('../../../../util/TextFormat');

    module.exports = React.createClass({

        getDefaultProps: function () {
            return {
                name: "",
                format: ""
            };
        },

        render: function () {
            return (
                <div>
                    Hello, {TextFormat.format(this.props.name, this.props.format)}
                </div>
            );
        }

    });

}());
