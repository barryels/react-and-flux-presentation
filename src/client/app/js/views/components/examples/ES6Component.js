/**
 * Created by barryels on 2015/04/03.
 */

import React, { PropTypes } from 'react';

class Hello extends React.Component {

    render() {
        return <div>Hello, {this.props.name}!</div>
    }

}
