/**
 * Created by barryels on 2015/04/09.
 */

(function () {
    'use strict';

    var React = require('react');
    var WebComponents = require('webcomponents.js/webcomponents');
    var qrCodeElement = require('webcomponent-qr-code/src/qr-code');

    module.exports = React.createClass({

        getDefaultProps: function () {
            return {
                qrCodeContent: ""
            };
        },

        getInitialState: function () {
            return {
                qrCodeContent: this.props.qrCodeContent
            };
        },

        handleChangeQRCodeText: function (e) {
            this.setState({
                qrCodeContent: e.target.value
            });
        },

        render: function () {
            return (
                <div>
                    <input type="text" onChange={this.handleChangeQRCodeText} value={this.state.qrCodeContent} />
                    <br /><br />
                    <qr-code ref="qrCodeElement" data={this.state.qrCodeContent} format="png"></qr-code>
                </div>
            );
        }
    });

}());
