/**
 * Created by barryels on 2015/04/03.
 */

(function () {
    'use strict';

    // You need to include 'react' as a base...
    // Why?
    // Because it's Awesome!
    var React = require('react');

    
    module.exports = React.createClass({


        // A string used in development mode console logging for easier debugging
        // By default, React will use the variable name used in the declaration of a React class, 
        // unless explicitly declared (as it is here).
        displayName: "HelloWorldComponent",


        // #Anti Pattern
        // A collection of mixins which add reusable functionality to component lifecycle methods
        // e.g. DateFormatterMixin, ValidatorMixin, etc.
        // Why it's an Anti Pattern:
        // ES6 wanted to implement the concept of mixins as a standard, but there were some issues... So as not to slow
        // down the "train release" cycle, mixins have been axed from the ES6 standard, but will be re-investigated 
        // in ES7. So, for now, use them, knowing that there will be some limitations.
        mixins: [],


        // #Anti Pattern
        // The statics object allows you to define static methods that can be called on the component class.
        // Why it's an Anti Pattern:
        // Allowing one component to 'talk' directly to another component introduces tight coupling. There will be 
        // instances where a components 'statics' API will change and come other component in another tree will break.
        // These breakages will be hard to debug and make your life hell.
        // If you use them, add a _TODO_, or whatever, that explicitly states your reliance on this components static 
        // methods 
        statics: {
            
        },


        // Invoked once and cached (immutable)
        // Used for property data validation (not UI validation) in development mode (not in production mode)
        propTypes: {
            name: React.PropTypes.string.isRequired
        },


        // Invoked once
        // Should return an immutable object which can later be referenced via `this.props`
        getDefaultProps: function () {
            console.log(this.displayName + ":", "getDefaultProps()");
            return {
                name: ""
            };
        },


        // Invoked once
        // Should returns a mutable object which can later be referenced via `this.state`
        getInitialState: function () {
            console.log("getInitialState()");
            return {
                name: this.props.name
            };
        },


        // Invoked once
        // Invoked immediately before the initial rendering occurs.
        componentWillMount: function () {
            console.log("componentWillMount()");
        },


        // Invoked once
        // immediately after the initial rendering occurs.
        // At this point in the lifecycle, the component has a DOM representation
        // which you can access via React.findDOMNode(this).
        componentDidMount: function () {
            console.log("componentDidMount()");
        },


        // Invoked immediately before a component is unmounted from the DOM.
        // Do any cleanup you need to do here, e.g. unbinding from event listeners, etc.
        componentWillUnmount: function () {
            console.log("componentWillUnmount()");
        },


        // Invoked when a component is receiving new props.
        // Not called for the initial render.
        // Mutate internal state from nextProps, using setState()
        componentWillReceiveProps: function (nextProps) {
            console.log("componentWillReceiveProps()", nextProps);
        },


        // 
        componentWillUpdate: function (nextProps, nextState) {
            console.log("componentWillUpdate()", nextProps, nextState);
        },


        // 
        componentDidUpdate: function (nextProps, nextState) {
            console.log("componentDidUpdate()", nextProps, nextState);
        },


        // Runs every time a change occurs to either `this.props` or `this.state`
        // Is a hook where you can write custom code to decide whether 
        // React should even consider diffing this component
        // Default implementation returns true
        shouldComponentUpdate: function(nextProps, nextState) {
            console.log("shouldComponentUpdate()", nextProps, nextState);
            return true;
        },


        // Describes what the UI should 'look' like
        // Uses `this.props` and `this.state` as inputs
        render: function () {
            
            console.log("render()", this.props, this.state);
            
            /**/
            // Vanilla JS syntax
            return (
                React.DOM.div(null,
                    React.createElement("input", {type: "text", onChange: this.handleChange, value: this.state.name}),
                    React.createElement("div", null, "Hello, ", this.state.name)
                )
            );
            /**/
            /**
            // JSX syntax
            return (
             <div>
                <input type="text" onChange={this.handleChange} value={this.state.name} />
                <div>Hello, {this.state.name}</div>
             </div>
            );
            /**/
        },

        /*
        Some public, non-lifecycle methods
        
        
        setState: function (newState) {
            this.state = newState;
        },


        findDOMNode: function (ref) {
            return <whatever></whatever>;
        },
        
        refs: array {
            return object;
        },
        
        */


        handleChange: function (e) {
            // e = SyntheticEvent
            console.log("handleChange()", e.target.value);
            this.setState({
                name: e.target.value
            });
        }
    
    });

}());