/**
 * Created by barryels on 2015/04/04.
 */

(function () {
    'use strict';

    var React = require('react');

    module.exports = React.createClass({
        
        getDefaultProps: function () {
            return {
                text: "",
                placeholder: "Search..."
            };
        },

        getInitialState: function () {
            return {
                text: this.props.text
            };
        },

        handleChange: function (e) {
            if (this.props.handleChange) {
                this.props.handleChange(e.target.value);
            }
            this.setState({
                text: e.target.value
            });
        },

        render: function () {
            return (
                <div className="TextFilter">
                    <input className="input form-control" type="text" onChange={this.handleChange} value={this.state.text} placeholder={this.props.placeholder} />
                </div>
            );
        }
    });
    

}());