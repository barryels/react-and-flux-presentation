/**
 * Created by barryels on 2015/04/04.
 */

(function () {
    'use strict';

    var React = require('react');
    var User = require('../../../domain/User');

    module.exports = React.createClass({

        displayName: "UserAddForm",

        getDefaultProps: function () {
            return {
                firstname: "",
                lastname: ""
            };
        },

        getInitialState: function () {
            return {
                firstname: this.props.firstname,
                lastname: this.props.lastname
            };
        },

        componentWillMount: function () {
            User.store.addListener(User.constants.CREATE_SUCCESS, this.onUserCreateSuccess);
            User.store.addListener(User.constants.CREATE_FAIL, this.onUserCreateFail);
        },

        componentWillUnount: function () {
            User.store.removeListener(User.constants.CREATE_SUCCESS);
            User.store.removeListener(User.constants.CREATE_FAIL);
        },

        onUserCreateSuccess: function () {
            //console.info("onUserCreateSuccess");
        },

        onUserCreateFail: function () {
            //console.info("onUserCreateFail");
        },

        handleFirstnameChange: function (e) {
            this.setState({
                firstname: e.target.value
            });
        },

        handleLastnameChange: function (e) {
            this.setState({
                lastname: e.target.value
            });
        },

        handleSubmit: function (e) {
            e.preventDefault();
            User.actions.createUser({
                firstName: this.state.firstname,
                lastName: this.state.lastname
            });
            this.setState({
                firstname: "",
                lastname: ""
            });
        },

        render: function () {
            return (
                <div>
                    <form className="form">
                        <div className="form-group">
                            <label>First Name:</label>
                            <input className="form-control" type="text" onChange={this.handleFirstnameChange} value={this.state.firstname} />
                        </div>
                        <div className="form-group">
                            <label>Last Name:</label>
                            <input className="form-control" type="text" onChange={this.handleLastnameChange} value={this.state.lastname} />
                        </div>
                        <button className="btn btn-primary btn-block" onClick={this.handleSubmit}>Add User</button>
                    </form>
                </div>
            );
        }
    });

}());
