/**
 * Created by barryels on 2015/04/03.
 */

(function () {
    'use strict';

    var React = require('react');

    module.exports = React.createClass({
        
        getDefaultProps: function () {
            return {
                
            };
        },
        
        getInitialState: function() {
            return {
                username: this.props.username,
                password: this.props.password
            }
        },

        handleUsernameChange: function (e) {
            this.setState({
                username: e.target.value
            })
        },
        handlePasswordChange: function (e) {
            this.setState({
                password: e.target.value
            })
        },
        handleSubmit: function (e) {
            e.preventDefault();
            console.info(this.state);
        },

        render: function () {

            return (
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input type="text" name="username" onChange={this.handleUsernameChange} value={this.state.username} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input type="password" name="password" onChange={this.handlePasswordChange} value={this.state.password} />
                        </div>
                        <button type="submit">Login</button>
                    </form>
                    
                </div>
            );
        }
    });

}());