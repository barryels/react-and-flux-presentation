/**
 * Created by barryels on 2015/04/03.
 */

(function () {
    'use strict';

    var React = require('react');

    var User = require('../../../domain/User');

    module.exports = React.createClass({

        getDefaultProps: function () {
            return {
                selectedUserId: -1
            }
        },

        getInitialState: function () {
            return {
                users: User.store.all(),
                selectedUserId: this.props.selectedUserId
            }
        },

        componentWillMount: function () {
            User.store.addListener(User.constants.USER_CHANGE, this.onUserStoreChange);

            User.actions.getAllUsers();
        },

        componentWillUnount: function () {
            User.store.removeListener(User.constants.USER_CHANGE);
        },


        onUserStoreChange: function () {
            this.setState({
                users: User.store.all()
            });
        },

        onSelectChange: function (e) {
            this.setState({
                selectedUserId: e.target.value
            });
        },

        render: function () {

            var options = [
                <option value="-1">Assign a user...</option>
            ];
            this.state.users.map(function (item, index) {
                options.push(
                    <option value={item._id}>{item.firstName + " " + item.lastName}</option>
                );
            });

            return (
                <div className="UserSelect">
                    <select className="form-control" onChange={this.onSelectChange} value={this.state.selectedUserId}>
                        {options}
                    </select>
                </div>
            );
        }
    });

}());
