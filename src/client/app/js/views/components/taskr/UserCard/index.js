/**
 * Created by barryels on 2015/04/15.
 */
(function () {
    'use strict';

    var React = require('react');

    var User = require('../../../../domain/User');

    module.exports = React.createClass({

        getDefaultProps: function () {
            return {
                model: {}
            }
        },

        getInitialState: function () {
            return {
                firstName: this.props.model.firstName,
                lastName: this.props.model.lastName,
                isEditing: false
            }
        },

        handleEditButtonClick: function (e) {
            this.setState({
                isEditing: true
            });
        },

        handleSave: function (e) {
            e.preventDefault();
            User.actions.updateUser({
                _id: this.props.model._id,
                firstName: this.state.firstName,
                lastName: this.state.lastName
            });
            this.setState({
                isEditing: false
            });
        },

        handleDeleteButtonClick: function (e) {
            User.actions.deleteUser({
                _id: this.props.model._id
            });
            this.setState({
                isEditing: false
            });
        },

        handleCancelButtonClick: function (e) {
            this.setState({
                firstName: this.props.model.firstName,
                lastName: this.props.model.lastName,
                isEditing: false
            });
        },

        handleFirstnameChange: function (e) {
            this.setState({
                firstName: e.target.value
            });
        },

        handleLastnameChange: function (e) {
            this.setState({
                lastName: e.target.value
            });
        },
        /*
         shouldComponentUpdate: function(nextProps, nextState) {
         return nextProps.id !== this.props.id;
         },
         */


        render: function () {

            var displayInterface = <div>
                <strong>{this.props.model.firstName} {this.props.model.lastName}</strong>
                <button className="btn btn-default" onClick={this.handleEditButtonClick}>Edit</button>
            </div>;

            var editInterface = <form className="form" onSubmit={this.handleSave}>
                <div className="form-group">
                    <input className="form-control" type="text" onChange={this.handleFirstnameChange} value={this.state.firstName} />
                    <input className="form-control" type="text" onChange={this.handleLastnameChange} value={this.state.lastName} />
                </div>
                <div className="form-group">
                    <button className="btn btn-default" onClick={this.handleCancelButtonClick}>Cancel</button>
                    <button className="btn btn-primary" type="submit">Save</button>
                    <button className="btn btn-danger" onClick={this.handleDeleteButtonClick}>Delete</button>
                </div>
            </form>

            if (!this.state.isEditing) {
                return (
                    <div className="UserCard">
                        {displayInterface}
                    </div>
                );
            } else {
                return (
                    <div className="UserCard">
                        {editInterface}
                    </div>
                );
            }
        }
    });

}());
