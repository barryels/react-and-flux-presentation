/**
 * Created by barryels on 2015/04/03.
 */

(function () {
    'use strict';

    var React = require('react');

    var Task = require('../../../../domain/Task');

    module.exports = React.createClass({

        getInitialState: function () {
            return {
                total: Task.store.all().length
            }
        },

        componentWillMount: function () {
            Task.store.addListener(Task.constants.TASK_CHANGE, this.onTaskStoreChange);

            Task.actions.readAll();
        },

        componentWillUnount: function () {
            Task.store.removeListener(Task.constants.TASK_CHANGE);
        },

        onTaskStoreChange: function () {
            this.setState({
                total: Task.store.all().length
            });
        },

        render: function () {
            return (
                <div className="TaskCounter">
                    <span className="total">Total tasks: {this.state.total}</span>
                </div>
            );
        }
    });

}());
