/**
 * Created by barryels on 2015/04/04.
 */

(function () {
    'use strict';

    var React = require('react');
    var moment = require('moment');

    var Task = require('../../../../domain/Task');
    var UserSelect = require('./../UserSelect');

    module.exports = React.createClass({

        getDefaultProps: function () {
            return {
                model: {}
            };
        },

        componentWillMount: function () {
            //Task.store.addListener(Task.constants.TASK_CHANGE, this.onTaskStoreChange);
        },

        componentWillUnount: function () {
            //Task.store.removeListener(Task.constants.TASK_CHANGE);
        },

        onTaskStoreChange: function () {

        },


        shouldComponentUpdate(nextProps, nextState) {
            if (nextProps.model.title === this.props.model.title) {
                //return false;
            }

            return true;
        },

        render: function () {
            return (
                <div className="TaskCard">
                    <div className="inner">
                        <h3 className="title">{this.props.model.title}</h3>
                        <div className="date-created">
                            Created: <div className="date">{moment(this.props.model.dateCreated).fromNow()}</div>
                        </div>
                        <div className="date-mod">
                            Edited: <div className="date">{moment(this.props.model.dateModified).fromNow()}</div>
                        </div>
                        <div className="importance">Importance: {this.props.model.random}</div>
                        <div className="footer">
                            <UserSelect />
                        </div>
                    </div>
                </div>
            );
        }
    });


}());
