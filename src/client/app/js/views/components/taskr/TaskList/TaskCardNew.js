/**
 * Created by barryels on 2015/04/04.
 */

(function () {
    'use strict';

    var React = require('react');
    var Task = require('../../../../domain/Task');

    module.exports = React.createClass({

        getInitialState: function () {
            return {
                title: "",
                busy: false
            };
        },

        componentWillMount: function () {
            Task.store.addListener(Task.constants.TASK_CREATE_START, this.handleTaskCreateStart);
            Task.store.addListener(Task.constants.TASK_CREATE_SUCCESS, this.handleTaskCreateSuccess);
            Task.store.addListener(Task.constants.TASK_CREATE_FAIL, this.handleTaskCreateFail);
        },

        componentWillUnount: function () {
            Task.store.removeListener(Task.constants.TASK_CREATE_START);
            Task.store.removeListener(Task.constants.TASK_CREATE_SUCCESS);
            Task.store.removeListener(Task.constants.TASK_CREATE_FAIL);
        },



        handleTaskCreateStart: function () {
            this.setState({
                busy: true
            });
        },

        handleTaskCreateFail: function () {
            //console.info("onUserCreateSuccess");
            this.setState({
                busy: false
            });
        },

        handleTaskCreateSuccess: function () {
            this.setState({
                title: "",
                busy: false
            });
        },

        handleTitleChange: function (e) {
            this.setState({
                title: e.target.value
            });
        },

        handleSubmit: function (e) {
            e.preventDefault();
            Task.actions.createTask({
                title: this.state.title
            });
        },

        render: function () {

            var className = "TaskCardNew";

            if (this.state.busy) {
                className += " busy";
            }

            return (
                <div className={className}>
                    <div className="inner">
                        <div className="front">
                            <h3 className="title">New Card</h3>
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <label>Title:</label>
                                    <input type="text" className="form-control" onChange={this.handleTitleChange} value={this.state.title} disabled={this.state.busy} />
                                </div>
                                <div className="form-group">
                                    <button className="btn btn-primary" type="submit" disabled={this.state.busy}>Create Task</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    });

}());
