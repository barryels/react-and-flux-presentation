/**
 * Created by barryels on 2015/04/03.
 */

(function () {
    'use strict';

    var React = require('react');

    module.exports = React.createClass({

        getDefaultProps: function () {
            return {
                type: "inline"
            }
        },
        
        getInitialState: function () {
            return {
                show: true,
                type: this.props.type
            }
        },
        
        render: function () {
            
            var className = "loadingIndicator "+ this.state.type;
            
            if (this.state.show) {
                return (
                    <div className={className}>
                        Loading...
                    </div>
                );
            } else {
                return null;
            }
        }
    });

}());