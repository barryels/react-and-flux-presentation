/**
 * Created by barryels on 2015/04/03.
 */

(function () {
    'use strict';

    var React = require('react');
    var ReactRouter = require('react-router');

    module.exports = React.createClass({

        getInitialState: function () {
            return {
                shouldShowHelloWorld: false
            }
        },
        
        handleToggleHelloWorldClick: function (e) {
            e.preventDefault();
            this.setState({
                shouldShowHelloWorld: !this.state.shouldShowHelloWorld
            })
        },
        
        render: function () {

            var helloWorldToggle = <button onClick={this.handleToggleHelloWorldClick}>Toggle "HelloWorld" Component</button>
            var helloWorld = null;
            
            if (this.state.shouldShowHelloWorld) {
                helloWorld = <HelloWorld />;
            }
            
            return (
                <div>

                </div>
            );
        }
    });

}());