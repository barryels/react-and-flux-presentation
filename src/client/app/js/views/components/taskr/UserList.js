/**
 * Created by barryels on 2015/04/04.
 */

(function () {
    'use strict';

    var React = require('react');
    var _ = require('lodash');

    var User = require('../../../domain/User');
    var UserCard = require('./UserCard');
    var TextFilter = require('./TextFilter');
    var LoadingIndicator = require('./LoadingIndicator');

    var UserList = React.createClass({

        getDefaultProps: function () {
            return {
                users: User.store.all()
            };
        },

        getInitialState: function () {
            return {
                users: this.props.users,
                usersFiltered: this.props.users,
                filterFullnameText: ""
            };
        },

        componentWillMount: function () {
            User.store.addListener(User.constants.USER_CHANGE, this.onUserStoreChange);

            User.actions.getAllUsers();
        },

        componentWillUnount: function () {
            User.store.removeListener(User.constants.USER_CHANGE);
        },

        onUserStoreChange: function () {
            this.setState({
                isLoading: false,
                users: User.store.all()
            });
        },

        handleTextFilterChange: function (filterTextValue) {
            this.filterByFullname(filterTextValue);
        },

        filterByFullname: function (filterTextValue) {
            var _usersFiltered = _.filter(this.state.users, function (user) {
                var fullname = user.firstName +" "+ user.lastName;
                if (fullname.toLowerCase().indexOf(filterTextValue.toLowerCase()) > -1) {
                    return true;
                }
            });

            this.setState({
                filterFullnameText: filterTextValue,
                usersFiltered: _usersFiltered
            });
        },

        render: function () {

            var loadingIndicator = <LoadingIndicator type="bar" />;
            if (!this.state.isLoading) {
                loadingIndicator = null;
            }

            var _users = this.state.users;
            if (this.state.filterFullnameText.length > 0) {
                _users = this.state.usersFiltered;
            }

            return (
                <div className="UserList">
                    <header className="header">
                        <TextFilter handleChange={this.handleTextFilterChange} placeholder="Filter users by name..." />
                    </header>

                    <div className="title">
                        User List: <strong>{_users.length}</strong>
                    </div>

                    {loadingIndicator}

                    <div className="list">
                    {_users.map(function(item, index) {
                        return (
                            <UserCard key={index} model={item} />
                        )
                    }, this)}
                    </div>
                </div>
            );
        }
    });

    module.exports = UserList;

}());
