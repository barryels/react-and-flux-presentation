/**
 * Created by barryels on 2015/04/04.
 */

(function () {
    'use strict';

    var React = require('react');
    var _ = require('lodash');

    var Task = require('../../../domain/Task');
    var User = require('../../../domain/User');
    var TextFilter = require('./TextFilter');
    var TaskCard = require('./TaskList/TaskCard');
    var TaskCardNew = require('./TaskList/TaskCardNew');

    module.exports = React.createClass({

        displayName: "TaskList",

        getInitialState: function () {
            return {
                tasks: Task.store.all(),
                tasksFiltered: Task.store.all(),
                filterTitleText: ""
            }
        },

        componentWillMount: function () {
            Task.store.addListener(Task.constants.TASK_CHANGE, this.onTaskStoreChange);

            Task.actions.readAll();
            //setInterval(Task.actions.readAll, 1000);
        },

        componentWillUnount: function () {
            Task.store.removeListener(Task.constants.TASK_CHANGE);
        },

        onTaskStoreChange: function () {
            var _tasksFiltered = Task.store.all();
            if (this.state.filterTitleText.length > 0) {
                _tasksFiltered = this.getFilteredTasks(this.state.filterTitleText);
            }
            this.setState({
                tasks: Task.store.all(),
                tasksFiltered: _tasksFiltered
            });
        },

        handleTextFilterChange: function (filterTextValue) {
            this.filterByTitle(filterTextValue);
        },

        getFilteredTasks: function (titleText) {
            var _tasksFiltered = _.filter(this.state.tasks, function (task) {
                if (task.title.toLowerCase().indexOf(titleText.toLowerCase()) > -1) {
                    return true;
                }
            });

            return _tasksFiltered;
        },

        filterByTitle: function (filterTextValue) {
            var _tasksFiltered = this.getFilteredTasks(filterTextValue);

            this.setState({
                filterTitleText: filterTextValue,
                tasksFiltered: _tasksFiltered
            });
        },

/*        shouldComponentUpdate: function (nextProps, nextState) {
            var randomDelta = Math.abs(nextState.random - this.state.random);
            if (randomDelta > 10) {
                return true;
            } else {
                return false;
            }
        },*/

        render: function () {
            var _tasks = this.state.tasks;
            if (this.state.filterTitleText.length > 0) {
                _tasks = this.state.tasksFiltered;
            }

            return (
                <div className="TaskList">

                    <header className="header">
                        <TextFilter handleChange={this.handleTextFilterChange} placeholder="Filter tasks by title..." />
                    </header>

                    <section className="list">
                        <TaskCardNew />

                        {_tasks.map(function(item, index) {
                            return (
                                <TaskCard key={index} model={item} />
                            )
                        }, this)}
                    </section>
                </div>
            );
        }
    });


}());
