var React = require('react');
var Router = require('react-router');

var LayoutUnauthenticated = React.createClass({
    render: function () {
        return (
            <div>
                <div>
                    <Router.RouteHandler />
                </div>
            </div>
        );
    }
});

module.exports = LayoutUnauthenticated;
