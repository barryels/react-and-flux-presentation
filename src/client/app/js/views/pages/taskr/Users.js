'use strict';

var React = require('react');
var UserList = require('../../components/taskr/UserList');
var UserNewForm = require('../../components/taskr/UserNewForm');

var PageUsers = React.createClass({

    render: function () {

        return (
            <div>

                <a href="#/home">Home</a>

                <h2>Users</h2>

                <div className="row">
                    <div className="col-sm-6 col-sm-offset-3">
                        <UserNewForm />
                    </div>
                </div>

                <hr />

                <UserList />


            </div>
        );
    }
});

module.exports = PageUsers;
