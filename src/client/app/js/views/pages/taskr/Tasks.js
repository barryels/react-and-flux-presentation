'use strict';

var React = require('react');
var TaskList = require('../../components/taskr/TaskList');

var PageTasks = React.createClass({

    render: function () {

        return (
            <div>

                <h2>Tasks</h2>

                <TaskList />

            </div>
        );
    }
});

module.exports = PageTasks;
