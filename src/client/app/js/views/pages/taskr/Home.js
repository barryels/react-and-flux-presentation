'use strict';

var React = require('react');

var TaskList = require('../../components/taskr/TaskList');
var TaskCounter = require('../../components/taskr/TaskCounter');

var PageHome = React.createClass({
    displayName: "PageHome",

    render: function () {
        return (
            <div>

                <a href="#/users">Users</a>

                <TaskCounter />
                <TaskList />
            </div>
        );
    }
});

module.exports = PageHome;
