'use strict';

var React = require('react');

var PageNotFound = React.createClass({

    displayName: "PageNotFound",

    render: function () {
        return (
            <div>
                <h1>Page Not Found</h1>
            </div>
        );
    }
});

module.exports = PageNotFound;