'use strict';

var React = require('react');

var LoginForm = require('../../components/taskr/LoginForm');

var PageLogin = React.createClass({

    render: function () {

        return (
            <div>

                <h2>Login</h2>

                <LoginForm />

            </div>
        );
    }
});

module.exports = PageLogin;
