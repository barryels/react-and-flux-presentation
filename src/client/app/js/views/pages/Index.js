'use strict';

var React = require('react');

var PageIndex = React.createClass({
    render: function () {
        return (
            <div>

                <ul className="examples-link-list">
                    <li>
                        <a href="#/examples">Examples</a>
                    </li>
                    <li>
                        <a href="#/home">Taskr App</a>
                    </li>
                </ul>
            </div>
        );
    }
});

module.exports = PageIndex;
