'use strict';

var React = require('react');

var SimpleForm = require('../../components/examples/DynamicForm');

var Page5 = React.createClass({

    handleSubmit: function (formData) {
        console.log(formData);
    },

    render: function () {

        return (
            <div>
                <SimpleForm handleSubmit={this.handleSubmit}/>
            </div>
        );
    }
});

module.exports = Page5;
