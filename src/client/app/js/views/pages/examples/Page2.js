'use strict';

var React = require('react');

var HelloWorldContainer = require('../../components/examples/HelloWorldContainer');

var Page2 = React.createClass({

    render: function () {

        return (
            <div>

                <h2>Example: Hello World (Container managed component)</h2>

                <HelloWorldContainer />

            </div>
        );
    }
});

module.exports = Page2;