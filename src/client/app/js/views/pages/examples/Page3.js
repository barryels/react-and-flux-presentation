'use strict';

var React = require('react');

var DragAndDrop = require('../../components/examples/DragAndDrop');

var Page3 = React.createClass({

    render: function () {

        return (
            <div>

                <DragAndDrop name="Draggable 1"/>
                <DragAndDrop name="Draggable 2" absolute={{x: 100, y: 100}}/>

            </div>
        );
    }
});

module.exports = Page3;
