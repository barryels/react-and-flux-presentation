'use strict';

var React = require('react');

var PageExamples = React.createClass({

    render: function () {

        return (
            <div>

                <h1>Examples</h1>

                <ul className="examples-link-list">
                    <li><a href="#examples/1">Hello World</a></li>
                    <li><a href="#examples/2">Hello World (Container Managed Component)</a></li>
                    <li><a href="#examples/3">Drag and Drop</a></li>
                    <li><a href="#examples/4">Web Components</a></li>
                    <li><a href="#examples/5">Dynamic Form</a></li>
                </ul>

            </div>
        );
    }
});

module.exports = PageExamples;
