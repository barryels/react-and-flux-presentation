'use strict';

var React = require('react');

var HelloWorld = require('../../components/examples/HelloWorld');

var Page1 = React.createClass({

    /*getDefaultProps: function () {
        return {
            punctuation: "!"
        };
    },*/

    render: function () {

        return (
            <div>
                <HelloWorld text="asdf" />
            </div>
        );
    }
});

module.exports = Page1;
