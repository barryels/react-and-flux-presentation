/**
 * Created by barryels on 2015/04/12.
 */
jest.autoMockOff();

describe('TextFormat', function() {

    it('should have a format function', function () {
        var TextFormat = require('../index');
        expect(TextFormat.format).not.toBe(undefined);
    });

    it('should return the original input value if the chosen formatter was not found', function () {
        var TextFormat = require('../index');
        expect(TextFormat.format("Some Text", "undefinedFormatter")).toBe("Some Text");
    });
    
    it('should format a string to uppercase', function () {
        var TextFormat = require('../index');
        expect(TextFormat.format("some text", "uppercase")).toBe("SOME TEXT");
        expect(TextFormat.format("some text", "UpperCase")).toBe("SOME TEXT");
    });
    
    it('should format a string to lowercase', function () {
        var TextFormat = require('../index');
        expect(TextFormat.format("sOme tExt", "lowercase")).toBe("some text");
    });

    it('should mask a string according to a builtin pattern type', function () {
        var TextFormat = require('../index');
        expect(TextFormat.format("SoMe Text 1", "mask")).toBe("**** **** *");
        expect(TextFormat.format("SoMe Text 2", "mask", {replace: "-"})).toBe("---- ---- -");
        expect(TextFormat.format("SoMe Text 3", "mask", {replace: "**"})).toBe("******** ******** **");
        expect(TextFormat.format("email1@domain.com", "mask", {type: "emailaddress"})).toBe("******@******.***");
    });

    // TODO: Fix this
    xit('should mask a string according to a given regexp pattern', function () {
        var TextFormat = require('../index');
        expect(TextFormat.format("SoMe Text 1", "mask", {pattern: /[^ ]/gi})).toBe("**** **** *");
        expect(TextFormat.format("No 1 Number5 2", "mask", {pattern: /[0-9]/gi})).toBe("No * Number* *");
        //expect(TextFormat.format("some text", "mask", {pattern: "^.{5}|(.*)", match: 1})).toBe("some ****");
    });

});