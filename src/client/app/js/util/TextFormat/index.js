/**
 * Created by barryels on 2015/04/12.
 */
(function () {
    'use strict';

    var _formatters = {

        "uppercase": function (input) {
            var output = input;
            return output.toUpperCase();
        },

        "lowercase": function (input) {
            var output = input;
            return output.toLowerCase();
        },

        "reverse": function (input) {
            var output = input;
            return output.split("").reverse();
        },

        "mask": function (input, options) {
            //console.log("----- Starting Mask ----->");
            var output = "",
                matches = [],
                pattern,
                patternTypes = {
                    "default": /[^ ]/gi,
                    "idnumber": /[^ ]/gi,
                    "emailaddress": /[\b\w]/gi,
                    "removespecial": /[^\b\w]/gi
                };

            if (!options) {
                options = {};
            }

            if (!options.type) {
                options.type = "default";
            }

            if (!options.pattern) {
                options.pattern = patternTypes[options.type];
            }

            //options.pattern = new RegExp(options.pattern);

            if (!options.replace) {
                options.replace = "*";
            }

            /*
            if (!options.match) {
                options.match = 0;
            }
            */

            //console.log("pattern:", options.pattern, "!");

            /*
            if (options.pattern === "") {
                matches = input.split("");
                matches.forEach(function (index, match) {
                    output += options.replace;//input.split("").join(options.replace);
                });

            } else {

            }
            */

            output = input.replace(options.pattern, function (match) {
                //console.log(match);
                return options.replace;
            });


            //console.log("<----- End Mask -----");

            return output;
        }

    };

    var _format = function (input, type, options) {
        var formatter = _findFormatter(type);
        var output = formatter(input, options);

        return output;
    };

    var _findFormatter = function (type) {
        var formatter = _formatters[type.toLowerCase()];

        if (!formatter) {
            //console.warn("TextFormat: Could not find formatter of type '" + type +"'. Using a basic one instead. It will simply return the original input value untouched.");
            formatter = function (input) {
                return input;
            }
        }

        return formatter;
    };

    module.exports = {
        format: function (input, type, options) {
            var output = _format(input, type, options);
            return output;
        }
    };

}());
