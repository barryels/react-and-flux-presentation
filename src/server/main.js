'use strict';

var restify = require('restify'),
    faker = require('faker'),
    request = require('request'),
    config = {
        port: 3006,
        url: "http://localhost:3006/"
    },
    userStore = require('save')('user'),
    taskStore = require('save')('task'),
    server;


server = restify.createServer({ name: 'api' });
server.use(restify.CORS());
server.use(restify.fullResponse());
server.use(restify.bodyParser());

/*
process.on('exit', function() {
    //process.exit(1);
    server.kill();
});
*/


var _generateRandomNumber = function (multiplier) {
    return Math.round(Math.random() * multiplier);
};



var bootstrap = function () {
    var i;
/*

    request({
        uri: config.url + "api/0.1/post",
        method: "POST",
        form: {
            data: "Barry"
        }
    }, function(error, response, body) {
        //console.log(error);
        //console.log(response);
        //console.log(body);
    });
*/

    for (i = 0; i < 10; i++) {
        request({
            uri: config.url + "api/0.1/tasks",
            method: "POST",
            form: {
                title: "#" + faker.random.number(10000) + " " + faker.lorem.sentence()
            }
        }, function(error, response, body) {
            //console.log(error);
            //console.log(response);
            //console.log(body);
        });
    }

    for (i = 0; i < 10; i++) {
        request({
            uri: config.url + "api/0.1/users",
            method: "POST",
            form: {
                firstName: faker.name.firstName(),
                email: faker.internet.email(),
                lastName: faker.name.lastName()
            }
        }, function(error, response, body) {
            //console.log(error);
            //console.log(response);
            //console.log(body);
        });
    }

};


server.listen(config.port, function () {
    console.log('%s listening at %s', server.name, server.url);
    bootstrap();
});


server.post('/api/0.1/post', function (req, res, next) {
    res.send(201, "Hello, " + req.params.data);
});

server.get('/api/0.1/get', function (req, res, next) {
    res.send(201, "pong");
});


server.get('/api/0.1/tasks', function (req, res, next) {
    console.log('Calling: /api/0.1/tasks @GET');
    var result;
    if (req.params._id) {
        taskStore.findOne({_id: req.params._id}, function (error, task) {
            res.send(201, {payload: task});
        });
    } else {
        taskStore.find({}, function (error, tasks) {
            tasks.forEach(function (item) {
                item.random = _generateRandomNumber(1000)
            });
            res.send(201, {payload: tasks});
        });
    }
});

server.post('/api/0.1/tasks', function (req, res, next) {
    taskStore.create({
        "title": req.params.title,
        "dateCreated": new Date().getTime(),
        "dateModified": new Date().getTime(),
        "random": _generateRandomNumber(1000)
    }, function (error, task) {
        if (error) {
            return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)));
        }
        res.send(201, {payload: task});
    });
});



server.get('/api/0.1/users', function (req, res, next) {
    console.log('Calling: /api/0.1/users @GET');
    var result;
    if (req.params._id) {
        userStore.findOne({_id: req.params._id}, function (error, user) {
            res.send(201, {payload: user});
        });
    } else {
        userStore.find({}, function (error, users) {
            res.send(201, {payload: users});
        });
    }
});

server.post('/api/0.1/users', function (req, res, next) {
    console.log('Calling: /api/0.1/users @POST');
    if (req.params.firstName === undefined) {
        return next(new restify.InvalidArgumentError('Name must be supplied'));
    }

    userStore.create({
        "firstName": req.params.firstName,
        "lastName": req.params.lastName,
        "createdAt": new Date().getTime()
    }, function (error, user) {
        if (error) {
            return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)));
        }
        res.send(201, {payload: user});
    });

});

server.put('/api/0.1/users', function (req, res, next) {
    console.log('Calling: /api/0.1/users @PUT');
    userStore.update(req.params, function (error, user) {
        res.send(201, {payload: user});
    });
});

server.del('/api/0.1/users', function (req, res, next) {
    console.log('Calling: /api/0.1/users @DELETE - '+ req.params.id);
    userStore.delete(req.params.id,
        function (error, user) {
            if (error) {
                return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)));
            }
            userStore.find({}, function (error, users) {
                res.send(201, {payload: users});
            });
        }
    );
});


