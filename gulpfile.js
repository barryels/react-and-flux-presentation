/**
 * Created by barryels on 2015/04/02.
 */

'use strict';

// Dependencies
var gulp = require('gulp'),
    buildConfig = require('./package.json')['buildConfig'],
    
    argv = require('yargs').argv,
    path = require('path'),
    del = require('del'),
    gulpIf = require("gulp-if"),
    colors = require('colors/safe'),
    
    gulpConcat = require("gulp-concat"),
    gulpUglify = require('gulp-uglify'),
    
    gulpLess = require("gulp-less"),
    gulpMinifyCSS = require("gulp-minify-css"),

    reactify = require('reactify'),
    browserify = require("browserify"),
    browserifyShim = require("browserify-shim"),
    watchify = require('watchify'),
    envify = require('envify'),
    es6ify = require('es6ify'),
    
    gulpJade = require('gulp-jade'),

    gulpConnect = require('gulp-connect'),

    vinylBuffer = require("vinyl-buffer"),
    vinylSourceStream = require("vinyl-source-stream");



gulp.task('dev:clean', function (cb) {
    /*
    del([
        buildConfig.paths.bundle.root + '**'
        //'!dist/mobile/deploy.json'
    ], cb);
    */
});

gulp.task('app-styles', function () {
    gulp.src([
        buildConfig.paths.app.styles.src + 'index.less'
    ])
        .pipe(gulpLess())
        .on('error', function(err) { console.log(err); })
        .pipe(gulpConcat("app-bundle.css"))
        .pipe(gulp.dest(buildConfig.paths.bundle.styles.dist));
});


function prependZero(item) {
    if (item.toString().length === 1) {
        item = "0" + item;
    }
    return item;
}

function formatDateToTimeString(date) {
    var timeString = "",
        hours = date.getHours(),
        minutes = date.getMinutes(),
        seconds = date.getSeconds();

    hours = prependZero(hours);
    minutes = prependZero(minutes);
    seconds = prependZero(seconds);
    
    timeString = hours + ":" + minutes + ":" + seconds;

    return timeString;
}

//console.log(colors);

function appScriptsBundler () {
    var target = (argv.target ? argv.target.toLowerCase() : false);
    if (!target) {
        target = "dev";
    }
    var watch = (target == "dev" ? true: false);
    
    console.log("Bundling scripts for: '" + target + "' with watch = " + watch);
    
    var bundler = browserify(buildConfig.paths.app.scripts.src + 'main.js', {
        debug: true,
        cache: {}, // required for watchify
        packageCache: {}, // required for watchify
        fullPaths: watch // required to be true only for watchify
    });

    if (watch) {
        bundler = watchify(bundler);
    }
    
    bundler.transform(reactify); // use the reactify transform
    
    //es6ify.traceurOverrides = {experimental: true};
    //bundler.transform(es6ify.configure(/.js/));
    
    bundler.transform({global: true}, envify);
    //b.add(buildConfig.paths.app.scripts.src + '../main.js', {debug: true});

    function rebundle() {
        var timeStart = new Date();
        console.log("[" + colors.grey(formatDateToTimeString(timeStart)) + "] Starting '" + colors.yellow("appScriptsBundler") + "' ...");
        
        var stream = bundler.bundle();
        stream.on('error', function(err) { console.log(err); });
        stream = stream.pipe(vinylSourceStream('app-bundle.js'));

        var timeEnd = new Date();
        var timeElapsed = timeEnd.getTime() - timeStart.getTime();
        console.log("[" + colors.grey(formatDateToTimeString(timeStart)) + "] Finished '" + colors.green("appScriptsBundler") + "' after " + colors.magenta(timeElapsed + " ms"));
        //[06:58:59] Finished 'app-styles' after 9.9 ms
        return stream.pipe(gulp.dest(buildConfig.paths.bundle.scripts.dist));
        /*
        stream.pipe(vinylSourceStream('app-bundle.js'))
        stream.on('error', function(err) { console.log(err); })
        stream.pipe(vinylBuffer())
        stream.on('error', function(err) { console.log(err); })
        stream.pipe(gulpIf(target === "production", gulpUglify()))
        stream.on('error', function(err) { console.log(err); })
        */
    };

    bundler.on('update', rebundle);
    return rebundle();
    
    //return bundler.bundle()
};

gulp.task('app-scripts', function() {
    return appScriptsBundler();
});

gulp.task('app-views', function () {
    gulp.src([
        buildConfig.paths.app.views.src + '**/!(_)*.jade'
    ])
        .pipe(gulpJade({
            pretty: true
        }))
        .on('error', function(err) { console.log(err); })
        .pipe(gulp.dest(buildConfig.paths.bundle.views.dist));
});



gulp.task('presentation-styles', function () {
    gulp.src([
        buildConfig.paths.presentation.styles.src + 'index.less'
    ])
        .pipe(gulpLess())
        .on('error', function(err) { console.log(err); })
        .pipe(gulpConcat("presentation-bundle.css"))
        .pipe(gulp.dest(buildConfig.paths.bundle.styles.dist));
});


gulp.task('presentation-scripts', function() {
    gulp.src(buildConfig.paths.presentation.scripts.src + 'presentation-bundle.js')
        .pipe(gulp.dest(buildConfig.paths.bundle.scripts.dist));
});


gulp.task('presentation-images', function() {
    gulp.src(buildConfig.paths.presentation.images.src + '**/*.{gif,jpg}')
        .pipe(gulp.dest(buildConfig.paths.bundle.images.dist +'presentation/'));
});

gulp.task('presentation-views', function () {
    gulp.src([
        buildConfig.paths.presentation.views.src + '**/!(_)*.jade'
    ])
        .pipe(gulpJade({
            pretty: true
        }))
        .on('error', function(err) { console.log(err); })
        .pipe(gulp.dest(buildConfig.paths.bundle.views.dist));
});


gulp.task('webserver', function () {
    var port = (argv.port ? argv.port : 8080);

    gulpConnect.server({
        root: buildConfig.paths.bundle.views.dist,
        port: port,
        livereload: false
    });

});


gulp.task('watch', function () {
    gulp.watch([
        buildConfig.paths.default.src + '**/*.less'
    ], ['app-styles', 'presentation-styles']);

    gulp.watch([
        './README.md',
        buildConfig.paths.default.src + '**/*.jade'
    ], ['app-views', 'presentation-views']);
});


gulp.task('presentation-all', ['presentation-styles', 'presentation-scripts', 'presentation-images', 'presentation-views']);
gulp.task('app-all', ['app-styles', 'app-scripts', 'app-views']);

// Default build task
gulp.task('dev:build', ['dev:clean', 'app-all', 'presentation-all']);

gulp.task('prod:build', ['prod:clean', 'app-all', 'presentation-all']);

// DEV: Build & Watch
gulp.task('dev:watch', ['default', 'watch']);

// DEV: Serve & Watch
gulp.task('dev:serve', ['default', 'watch', 'webserver']);

// Default task
gulp.task('default', ['dev:build']);